package com.kwapinspire.kwap;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Profile;
import com.kwapinspire.kwap.model.UpdateProfile;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.restinterface.ServiceGenerator;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.SettingsPreferences;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by My 7 on 15-May-18.
 */

public class UpdateProfileActivity extends BaseActivity {
    UpdateProfileActivity mContext;

    boolean isInternet;

    LinearLayout update_profile_main;

    TextView txt_toolbar_title, txt_profileinfo, text_title_full_name, text_title_email, text_title_phone, text_title_company,
            text_title_position, text_title_country, txt_changepassword, text_title_new_pass, text_title_verify_pass;
    EditText edt_full_name, edt_email, edt_phone, edt_company, edt_position, edt_country, edt_new_pass, edt_verify_pass;

    CircularImageView image_user;

    Button btn_update_profile;

    ProgressBar update_profile_progress;

    //For image selection from gallery
    private static final int PICK_IMAGE = 1;
    Bitmap bitmap, personBitmap;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    String filemanagerstring, selectedImagePath = null;

    static boolean isImage = false;

    TypedFile typedFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_profile);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setFonts();
        setListener();

        if (isInternet) {
            getProfileRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        update_profile_main = findViewById(R.id.update_profile_main);
        image_user = findViewById(R.id.image_user);
        btn_update_profile = findViewById(R.id.btn_update_profile);

        update_profile_progress = findViewById(R.id.update_profile_progress);

        showTextViewsAsMandatory((TextView) findViewById(R.id.text_title_full_name), getResources().getString(R.string.full_name));
        showTextViewsAsMandatory((TextView) findViewById(R.id.text_title_email), getResources().getString(R.string.email));
        showTextViewsAsMandatory((TextView) findViewById(R.id.text_title_phone), getResources().getString(R.string.phone));
        showTextViewsAsMandatory((TextView) findViewById(R.id.text_title_company), getResources().getString(R.string.company));
        showTextViewsAsMandatory((TextView) findViewById(R.id.text_title_position), getResources().getString(R.string.position));
        showTextViewsAsMandatory((TextView) findViewById(R.id.text_title_country), getResources().getString(R.string.country));

        edt_full_name = findViewById(R.id.edt_full_name);
        edt_email = findViewById(R.id.edt_email);
        edt_phone = findViewById(R.id.edt_phone);
        edt_company = findViewById(R.id.edt_company);
        edt_position = findViewById(R.id.edt_position);
        edt_country = findViewById(R.id.edt_country);
        edt_new_pass = findViewById(R.id.edt_new_pass);
        edt_verify_pass = findViewById(R.id.edt_verify_pass);

        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        txt_profileinfo = findViewById(R.id.txt_profileinfo);
        text_title_full_name = findViewById(R.id.text_title_full_name);
        text_title_email = findViewById(R.id.text_title_email);

        text_title_phone = findViewById(R.id.text_title_phone);
        text_title_company = findViewById(R.id.text_title_company);
        text_title_position = findViewById(R.id.text_title_position);
        text_title_country = findViewById(R.id.text_title_country);
        txt_changepassword = findViewById(R.id.txt_changepassword);
        text_title_new_pass = findViewById(R.id.text_title_new_pass);
        text_title_verify_pass = findViewById(R.id.text_title_verify_pass);

//        overrideFonts(update_profile_main);

    }

    private void setFonts(){

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_profileinfo.setTypeface(getRegularFonts());
        txt_changepassword.setTypeface(getRegularFonts());

        text_title_full_name.setTypeface(getRegularFonts());
        edt_full_name.setTypeface(getRegularFonts());
        text_title_email.setTypeface(getRegularFonts());
        edt_email.setTypeface(getRegularFonts());

        text_title_phone.setTypeface(getRegularFonts());
        edt_phone.setTypeface(getRegularFonts());
        text_title_company.setTypeface(getRegularFonts());
        edt_company.setTypeface(getRegularFonts());

        text_title_position.setTypeface(getRegularFonts());
        edt_position.setTypeface(getRegularFonts());
        text_title_country.setTypeface(getRegularFonts());
        edt_country.setTypeface(getRegularFonts());

        text_title_new_pass.setTypeface(getRegularFonts());
        edt_new_pass.setTypeface(getRegularFonts());
        text_title_verify_pass.setTypeface(getRegularFonts());
        edt_verify_pass.setTypeface(getRegularFonts());

        btn_update_profile.setTypeface(getRegularFonts());
    }

    public void showTextViewsAsMandatory(TextView tv, String title) {
        tv.setText(Html.fromHtml(title + "<font color=\"#ff0000\">" + "*" + "</font>"));
    }

    private void setListener() {
        image_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    AskForPermission();
                } else {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }
            }
        });

        btn_update_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edt_full_name.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please fill up all required fields!!", Toast.LENGTH_LONG).show();
                } else if (edt_email.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please fill up all required fields!!", Toast.LENGTH_LONG).show();
                } else if (!isValidEmail(edt_email.getText().toString())) {
                    Toast.makeText(mContext, "Please enter valid email address!!", Toast.LENGTH_LONG).show();
                } else if (edt_phone.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please fill up all required fields!!", Toast.LENGTH_LONG).show();
                } else if (edt_company.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please fill up all required fields!!", Toast.LENGTH_LONG).show();
                } else if (edt_position.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please fill up all required fields!!", Toast.LENGTH_LONG).show();
                } else if (edt_country.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please fill up all required fields!!", Toast.LENGTH_LONG).show();
                } else if (!edt_new_pass.getText().toString().equalsIgnoreCase(edt_verify_pass.getText().toString())) {
                    Toast.makeText(mContext, "Your password must be same as above", Toast.LENGTH_LONG).show();
                } else if (selectedImagePath == null && isImage)//
                {
                    Toast.makeText(mContext, "Please select your profile picture!!", Toast.LENGTH_LONG).show();
                } else {
                    if (isInternet) {
                        sendUpdateProfileRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void getProfileRequest() {
        try {
            String userId = getUserId();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.GetProfileRequest(userId, new Callback<Profile>() {
                @Override
                public void success(Profile model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("PROFILE_RESPONSE", "" + new Gson().toJson(model));

                            //if successfully login then status 1 else 0
                            if (model.getStatus() == 1) {
                                Log.e("USER_IMAGE", "" + model.getProfileImage());

                                SettingsPreferences.storeConsumer(mContext, model);

                                setUserData();
                            } else {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUserData() {
        Profile data = SettingsPreferences.getConsumer(mContext);

        edt_full_name.setText(data.getName());
        edt_email.setText(data.getEmail());
        edt_phone.setText(data.getPhone());
        edt_company.setText(data.getCompany());
        edt_position.setText(data.getPosition());
        edt_country.setText(data.getCountry());

        if (!data.getProfileImage().equalsIgnoreCase("")) {
            Glide.with(mContext).load(data.getProfileImage())
                    .error(R.drawable.ic_logo_profile)
                    .listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    update_profile_progress.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    update_profile_progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(image_user);
        } else {
            update_profile_progress.setVisibility(View.GONE);
            image_user.setImageResource(R.drawable.ic_logo_profile);
        }

        Log.e("PROFILE_IMAGE", "" + data.getProfileImage());
        if (data.getProfileImage().equals(null)) {
            isImage = true;
        }
    }

    private void sendUpdateProfileRequest() {
        if (selectedImagePath != null) {
            typedFile = new TypedFile("multipart/form-data", new File(selectedImagePath));
            Log.e("User_Image", "" + selectedImagePath);
        }

        String userId = getUserId();
        String name = edt_full_name.getText().toString();
        String email = edt_email.getText().toString();
        String phone = edt_phone.getText().toString();
        String company = edt_company.getText().toString();
        String position = edt_position.getText().toString();
        String country = edt_country.getText().toString();
        String password = edt_new_pass.getText().toString();

        Log.e("userId", "" + userId);
        Log.e("name", "" + name);
        Log.e("email", "" + email);
        Log.e("phone", "" + phone);
        Log.e("company", "" + company);
        Log.e("position", "" + position);
        Log.e("country", "" + country);
        Log.e("password", "" + password);

        mContext.showWaitIndicator(true);

        RestInterface service = ServiceGenerator.createService(RestInterface.class);
        service.updateProfileRequest(typedFile, userId, name, email, phone, company, position, country, password, new Callback<UpdateProfile>() {
            @Override
            public void success(UpdateProfile model, Response response) {
                mContext.showWaitIndicator(false);
                Log.e("UPDATE_PROFILE_RESPONSE", "" + new Gson().toJson(model));

                if (response.getStatus() == 200) {
                    if (model.getStatus() == 1) {
                        Toast.makeText(mContext, "" + model.getMessage(), Toast.LENGTH_LONG).show();

                        saveFreshInstall(false);
                        startActivity(new Intent(mContext, DashBoardActivity.class));
                        finish();
                    } else {
                        Toast.makeText(mContext, "" + model.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mContext.showWaitIndicator(false);
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void AskForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int requestcode, Intent data) {
        super.onActivityResult(requestCode, requestcode, data);

        if (requestcode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            String filePath = null;

            try {
                // OI FILE Manager
                filemanagerstring = selectedImageUri.getPath();

                // MEDIA GALLERY
                selectedImagePath = getPath(selectedImageUri);
                Log.e("selectedImagePath", "path : " + selectedImagePath);

                if (selectedImagePath != null) {
                    filePath = selectedImagePath;
                } else if (filemanagerstring != null) {
                    filePath = filemanagerstring;
                } else {
                    Toast.makeText(mContext, "Unknown path", Toast.LENGTH_LONG).show();
                    Log.e("Bitmap", "Unknown path");
                }

                if (filePath != null) {
                    bitmap = BitmapFactory.decodeFile(filePath);
                    image_user.setImageBitmap(bitmap);
                    isImage = false;
                } else {
                    bitmap = null;
                }

            } catch (Exception e) {
                Toast.makeText(mContext, "Internal Error", Toast.LENGTH_LONG).show();
                Log.e(e.getClass().getName(), e.getMessage(), e);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
