package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.adapters.SurveyAdapter;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Survey;
import com.kwapinspire.kwap.model.SurveyAnswers;
import com.kwapinspire.kwap.model.SurveyData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.SpacesItemDecoration;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EventSurveyActivity extends BaseActivity {

    EventSurveyActivity mContext;

    LinearLayout ll_parent_event_survey, ll_event_survey_back,layout_survey,layout_thank_u_survey;

    TextView txt_toolbar_title, txt_event_survey_header;
    RecyclerView rv_list_survey;
    Button btn_submit;

    boolean isInternet;
    String Answers = "NULL";

    ArrayList<SurveyData> surveyData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_survey);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListeners();

        if (isInternet) {
            getSurveyRequest();
        }
        else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        txt_event_survey_header = findViewById(R.id.txt_event_survey_header);

        ll_parent_event_survey = findViewById(R.id.ll_parent_event_survey);
        ll_event_survey_back = findViewById(R.id.ll_event_survey_back);
        layout_survey = findViewById(R.id.layout_survey);
        layout_thank_u_survey = findViewById(R.id.layout_thank_u_survey);

        btn_submit = findViewById(R.id.btn_submit);

        rv_list_survey = findViewById(R.id.rv_list_survey);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rv_list_survey.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_event_survey_header.setTypeface(getSemiBoldFonts());
        btn_submit.setTypeface(getRegularFonts());
    }

    private void setListeners(){

        ll_event_survey_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSurveyData();
            }
        });
    }

    public void getSurveyRequest()
    {
        Log.e("UserId",""+getUserId());

        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getSurveyRequest(getUserId(),new Callback<Survey>() {
            @Override
            public void success(Survey model, Response response) {

                if (response.getStatus() == 200) {
                    Log.e("SURVEY_RESPONSE", "" + new Gson().toJson(model));

                    try {
                        showWaitIndicator(false);

                        layout_survey.setVisibility(View.VISIBLE);
                        layout_thank_u_survey.setVisibility(View.GONE);

                        surveyData = model.getData();
                        setSurveyData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void setSurveyData()
    {
        SurveyAdapter adapter = new SurveyAdapter(mContext,surveyData);
        rv_list_survey.setLayoutManager(new LinearLayoutManager(mContext));
        rv_list_survey.getLayoutManager().setMeasurementCacheEnabled(false);
        rv_list_survey.setAdapter(adapter);
        rv_list_survey.invalidate();
    }

    private void getSurveyData()
    {
        String tempsrt = null;

        StringBuilder commaSepValueBuilder = new StringBuilder();

        ArrayList<String> tempSurveyAns = new ArrayList<>();

        Log.e("Size of Array", ""+surveyData.size());

        for(int i = 0; i< surveyData.size(); i++){

            tempsrt = surveyData.get(i).getSurveyId()+":"+surveyData.get(i).getAnswer() ;

            Log.e("", "Answers : "+tempsrt);

            tempSurveyAns.add(tempsrt);
        }

        //Looping through the list
        for ( int i = 0; i< tempSurveyAns.size(); i++){
            //append the value into the builder
            commaSepValueBuilder.append(tempSurveyAns.get(i));

            //if the value is not the last element of the list
            //then append the comma(,) as well
            if ( i != tempSurveyAns.size()-1){
                commaSepValueBuilder.append("|");
            }
        }

        Answers = commaSepValueBuilder.toString().trim();
        System.out.println(commaSepValueBuilder.toString().trim());
        Log.e("Answers",""+Answers);

        /*if(tempsrt.contains("null"))
        {
            Toast.makeText(mContext, "Please fill all survey answers!!", Toast.LENGTH_LONG).show();
        }
        else
        {*/
        if (isInternet) {
            sendSurveyRequest();
        }
        else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
//        }
    }

    private void sendSurveyRequest()
    {
        Log.e("UserId",""+getUserId());
        Log.e("Answers",""+Answers);

        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.sendSurveyAnswers(getUserId(),Answers,new Callback<SurveyAnswers>() {
            @Override
            public void success(SurveyAnswers model, Response response) {

                if (response.getStatus() == 200) {
                    Log.e("SERVEY_ANSWERS", "" + new Gson().toJson(model));
                    showWaitIndicator(false);

                    try {
                        if(model.getStatus() == 1)
                        {
                            layout_survey.setVisibility(View.GONE);
                            layout_thank_u_survey.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            layout_thank_u_survey.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
