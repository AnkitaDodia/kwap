package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.adapters.SponsorsAdapter;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Sponsors;
import com.kwapinspire.kwap.model.SponsorsData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.SpacesItemDecoration;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 16-May-18.
 */

public class SponsorsActivity extends BaseActivity
{
    SponsorsActivity mContext;

    boolean isInternet;

    LinearLayout sponsors_main, ll_sponsors_back,error_layout_sponsors;

    RecyclerView rv_list_sponsors;

    TextView text_error_sponsors;

    String error;

    ArrayList<SponsorsData> sponsorsData = new ArrayList<>();

    SponsorsAdapter adapter;
    TextView txt_toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListener();
//        overrideFonts(sponsors_main);

        if(BaseActivity.sponsorsData.size() > 0)
        {
            sponsorsData.clear();
            sponsorsData = BaseActivity.sponsorsData;

            setSponsorsAdapter();
        }
        else
        {
            if (isInternet) {
                sendSponsersRequest();
            }
            else {
                Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        sponsors_main = findViewById(R.id.sponsors_main);
        ll_sponsors_back = findViewById(R.id.ll_sponsors_back);
        error_layout_sponsors = findViewById(R.id.error_layout_sponsors);

        text_error_sponsors = findViewById(R.id.text_error_sponsors);

        rv_list_sponsors = findViewById(R.id.rv_list_sponsors);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rv_list_sponsors.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        txt_toolbar_title.setTypeface(getMediumFonts());
    }

    private void setListener() {
        ll_sponsors_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });
    }

    private void sendSponsersRequest()
    {
        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getSponsors(new Callback<Sponsors>() {
            @Override
            public void success(Sponsors data, Response response) {

                if (response.getStatus() == 200) {
                    try {
                        Log.e("SPONSORS_RESPONSE", "" + new Gson().toJson(data));
                        showWaitIndicator(false);

                        if(data.getStatus()== 1){
                            sponsorsData = data.getData();

                            BaseActivity.sponsorsData = sponsorsData;

                        }else{
                            error = data.getMessage();
                        }

                        setSponsorsAdapter();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void setSponsorsAdapter()
    {
        if(sponsorsData.size() > 0)
        {
            error_layout_sponsors.setVisibility(View.GONE);
            rv_list_sponsors.setVisibility(View.VISIBLE);

            adapter = new SponsorsAdapter(mContext,sponsorsData);
            rv_list_sponsors.setLayoutManager(new LinearLayoutManager(mContext));
            rv_list_sponsors.getLayoutManager().setMeasurementCacheEnabled(false);
//            DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
//            rv_list_sponsors.addItemDecoration(itemDecor);
            rv_list_sponsors.setAdapter(adapter);
        }
        else
        {
            text_error_sponsors.setText(error);
            error_layout_sponsors.setVisibility(View.VISIBLE);
            rv_list_sponsors.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
