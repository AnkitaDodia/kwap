package com.kwapinspire.kwap.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Scroller;
import android.widget.TextView;

import com.kwapinspire.kwap.EventSurveyActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.model.SurveyData;

import java.util.ArrayList;

/**
 * Created by My 7 on 21-May-18.
 */

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.SurveyViewHolder>
{
    EventSurveyActivity mContext;

    ArrayList<SurveyData> mList = new ArrayList<>();

//    SurveyData mSurveyData;

    public SurveyAdapter(EventSurveyActivity mContext, ArrayList<SurveyData> SurveyList)
    {
        this.mContext = mContext;
        this.mList = SurveyList;
    }

    @Override
    public SurveyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_survey, null);

        return new SurveyAdapter.SurveyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SurveyViewHolder holder, int position) {
        final SurveyData mSurveyData = mList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.survey_list_main.setLayoutParams(params);

        int queNumber = position + 1;
        holder.text_que_id.setText(queNumber+".");
        holder.text_que.setText(mSurveyData.getQuestion());

        holder.text_que_id.setTypeface(mContext.getRegularFonts());
        holder.text_que.setTypeface(mContext.getRegularFonts());

        if(mSurveyData.getType().equalsIgnoreCase("star"))
        {
            holder.edt_msg_survey.setVisibility(View.GONE);
            holder.ratingBar_user_rating.setVisibility(View.VISIBLE);

            holder.ratingBar_user_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    mSurveyData.setAnswer(Float.toString(v));
                }
            });

            try
            {
                if(mSurveyData.getAnswer() != null){
                    holder.ratingBar_user_rating.setRating(Float.valueOf(mSurveyData.getAnswer()));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            holder.edt_msg_survey.setTypeface(mContext.getRegularFonts());
            holder.edt_msg_survey.setVisibility(View.VISIBLE);
            holder.ratingBar_user_rating.setVisibility(View.GONE);

            if(mSurveyData.getAnswer() != null && !mSurveyData.getAnswer().equalsIgnoreCase("0.0")){

                Log.e("getAnswer()",""+mSurveyData.getAnswer());

                holder.edt_msg_survey.setText(mSurveyData.getAnswer());
            }
            holder.edt_msg_survey.setMovementMethod(new ScrollingMovementMethod());
//            holder.myCustomEditTextListener.updatePosition(position);
            holder.edt_msg_survey.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    mSurveyData.setAnswer(editable.toString());
                }
            });

            holder.edt_msg_survey.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class SurveyViewHolder extends RecyclerView.ViewHolder {

        TextView text_que_id,text_que;

        LinearLayout survey_list_main;

        RatingBar ratingBar_user_rating;

        EditText edt_msg_survey;

//        public CustomEtListener myCustomEditTextListener;

        public SurveyViewHolder(View convertView) {
            super(convertView);

            text_que_id = convertView.findViewById(R.id.text_que_id);
            text_que = convertView.findViewById(R.id.text_que);

            ratingBar_user_rating = convertView.findViewById(R.id.ratingBar_user_rating);

            edt_msg_survey = convertView.findViewById(R.id.edt_msg_survey);

            survey_list_main  = convertView.findViewById(R.id.survey_list_main);

//            myCustomEditTextListener = myLis;
//            edt_msg_survey.addTextChangedListener(myCustomEditTextListener);
        }
    }

    /*private class CustomEtListener implements TextWatcher {
        private int position;


        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            mSurveyData.setAnswer(editable.toString());
        }
    }*/
}
