package com.kwapinspire.kwap.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kwapinspire.kwap.AgendaDetailsActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.model.AgendaSpeakers;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AgendaSpeakerAdapter extends RecyclerView.Adapter<AgendaSpeakerAdapter.MyViewHolder> {

    ArrayList<AgendaSpeakers> mAgendaSpeakersList = new ArrayList<>();
    AgendaDetailsActivity mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_agenda_speaker, txt_agenda_speaker_dis;
        CircleImageView img_agenda_speaker;
        ProgressBar agenda_details_progress;
        LinearLayout agenda_speaker_main;
//        WebView webview_agenda_speaker_dis;

        public MyViewHolder(View convertView) {
            super(convertView);

            agenda_speaker_main = convertView.findViewById(R.id.agenda_speaker_main);

            txt_agenda_speaker =  convertView.findViewById(R.id.txt_agenda_speaker);
            txt_agenda_speaker_dis = convertView.findViewById(R.id.txt_agenda_speaker_dis);

            img_agenda_speaker = convertView.findViewById(R.id.img_agenda_speaker);
            agenda_details_progress = convertView.findViewById(R.id.agenda_details_progress);

//            webview_agenda_speaker_dis = convertView.findViewById(R.id.webview_agenda_speaker_dis);
        }
    }

    public AgendaSpeakerAdapter(AgendaDetailsActivity ctx, ArrayList<AgendaSpeakers> AgendaSpeakersList) {
        this.mAgendaSpeakersList = AgendaSpeakersList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_agendaspeaker, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        AgendaSpeakers mAgendaSpeakers = mAgendaSpeakersList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.agenda_speaker_main.setLayoutParams(params);

        holder.txt_agenda_speaker.setText(mAgendaSpeakers.getName());
        holder.txt_agenda_speaker_dis.setText(mAgendaSpeakers.getPosition());

        holder.txt_agenda_speaker.setTypeface(mContext.getMediumFonts());
        holder.txt_agenda_speaker_dis.setTypeface(mContext.getLightFonts());

//        Log.e("discription", "discription  = "+mAgendaSpeakers.getDescription());
//
//        holder.webview_agenda_speaker_dis.loadDataWithBaseURL("file:///android_asset/",
//                getStyledFont(mAgendaSpeakers.getDescription()),
//                "text/html; charset=UTF-8", null, "about:blank");

        String imgUrl = mAgendaSpeakers.getImage();

        Log.e("imgUrl", "imgUrl = "+imgUrl);


        Glide.with(mContext).load(mAgendaSpeakers.getImage())
                .error(R.drawable.ic_logo_profile)
                .listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.agenda_details_progress.setVisibility(View.GONE);
                holder.img_agenda_speaker.setImageResource(R.drawable.ic_logo_profile);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.agenda_details_progress.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.img_agenda_speaker);
    }

    @Override
    public int getItemCount() {
        return mAgendaSpeakersList.size();
    }
}