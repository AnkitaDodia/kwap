package com.kwapinspire.kwap.adapters;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kwapinspire.kwap.AgendaActivity;
import com.kwapinspire.kwap.AgendaDetailsActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Day;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by My 7 on 19-May-18.
 */

public class Day2AgendaAdapter extends RecyclerView.Adapter<Day2AgendaAdapter.AgendaViewHolder>
{
    AgendaActivity mContext;

    ArrayList<Day> mList = new ArrayList<>();


    public Day2AgendaAdapter(AgendaActivity mContext, ArrayList<Day> mDayList)
    {
        this.mContext = mContext;
        mList = mDayList;
    }

    @Override
    public AgendaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_agenda, null);

        return new Day2AgendaAdapter.AgendaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AgendaViewHolder holder, final int position) {
        Day data = mList.get(position);

        holder.text_agenda_title.setText(data.getTitle());
        holder.text_agenda_time.setText(data.getFromTime()+" - "+data.getToTime());

        holder.text_agenda_title.setTypeface(mContext.getLightFonts());
        holder.text_agenda_time.setTypeface(mContext.getSemiBoldFonts());
        holder.btn_read_more.setTypeface(mContext.getRegularFonts());

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.layout_list_agenda_main.setLayoutParams(params);

        Log.e("TITLE",""+data.getTitle());

        Log.e("COLOR",""+data.getBackgroundColor());

        if(!data.getBackgroundColor().equalsIgnoreCase(""))
        {
            holder.layout_list_agenda_main.setBackgroundColor(Color.parseColor(data.getBackgroundColor()));
        }
        else
        {
            holder.layout_list_agenda_main.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        if(!data.getDescription().equalsIgnoreCase(""))
        {
            holder.btn_read_more.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.btn_read_more.setVisibility(View.GONE);
        }

        Glide.with(mContext)
                .load(data.getImage())
                .error(R.drawable.ic_image_holder)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.agenda_progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.agenda_progress.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.img_agenda);

        holder.btn_read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.AgendaId = BaseActivity.mDay2List.get(position).getAgendaId();
                Intent it = new Intent(mContext, AgendaDetailsActivity.class);
                mContext.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class AgendaViewHolder extends RecyclerView.ViewHolder {

        TextView text_agenda_title,text_agenda_time;

        ImageView img_agenda;

        ProgressBar agenda_progress;

        LinearLayout layout_list_agenda_main;

        Button btn_read_more;

        public AgendaViewHolder(View convertView) {
            super(convertView);

            img_agenda = convertView.findViewById(R.id.img_agenda);

            agenda_progress = convertView.findViewById(R.id.agenda_progress);

            text_agenda_title = convertView.findViewById(R.id.text_agenda_title);
            text_agenda_time = convertView.findViewById(R.id.text_agenda_time);

            layout_list_agenda_main  = convertView.findViewById(R.id.layout_list_agenda_main);
//            mContext.overrideFonts(layout_list_agenda_main);

            btn_read_more = convertView.findViewById(R.id.btn_read_more);
        }
    }
}
