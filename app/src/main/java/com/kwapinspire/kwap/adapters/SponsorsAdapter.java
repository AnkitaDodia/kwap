package com.kwapinspire.kwap.adapters;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.SponsorsActivity;
import com.kwapinspire.kwap.model.SponsorsData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by My 7 on 17-May-18.
 */

public class SponsorsAdapter extends RecyclerView.Adapter<SponsorsAdapter.SponsorsAdapterHolder>
{
    SponsorsActivity mContext;

    ArrayList<SponsorsData> mData = new ArrayList<>();

    public class SponsorsAdapterHolder extends RecyclerView.ViewHolder
    {
        TextView text_sponsor_name,text_sponsor_dis,text_sponsor_website;

        LinearLayout sponsors_list_main;

        ImageView img_sponsors;

        ProgressBar progress_sponsors;

        public SponsorsAdapterHolder(View view) {
            super(view);
//            text_sponsor_name = view.findViewById(R.id.text_sponsor_name);
//            text_sponsor_dis = view.findViewById(R.id.text_sponsor_dis);
//            text_sponsor_website = view.findViewById(R.id.text_sponsor_website);

            sponsors_list_main = view.findViewById(R.id.layout_participant_row_main);
//            mContext.overrideFonts(sponsors_list_main);

            img_sponsors = view.findViewById(R.id.img_sponsors);

            progress_sponsors = view.findViewById(R.id.progress_sponsors);
        }
    }

    public SponsorsAdapter(SponsorsActivity mContext, ArrayList<SponsorsData> mList) {
        this.mData = mList;
        this.mContext = mContext;
    }

    @Override
    public SponsorsAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_list_sponsors, parent, false);
        SponsorsAdapterHolder Vh = new SponsorsAdapterHolder(itemView);
        return Vh;
    }

    @Override
    public void onBindViewHolder(final SponsorsAdapterHolder holder, final int position) {
        SponsorsData data = mData.get(position);

//        holder.text_sponsor_name.setText(data.getName());
//        holder.text_sponsor_dis.setText(data.getDescription());
//        holder.text_sponsor_website.setText(data.getLink());

        Log.e("SPONSORE",""+data.getImage());
        Glide.with(mContext)
                .load(data.getImage())
                .error(R.drawable.ic_image_holder)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progress_sponsors.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progress_sponsors.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.img_sponsors);

        holder.img_sponsors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mData.get(position).getLink())));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
