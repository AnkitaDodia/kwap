package com.kwapinspire.kwap.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kwapinspire.kwap.NotificationActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.NotificationData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder>  {

    ArrayList<NotificationData> mNotificationList = new ArrayList<>();

    NotificationActivity mContext;

    String des;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_notification_title, txt_notification_discription, txt_notification_time;
        LinearLayout ll_notification_row_parent;

        public MyViewHolder(View convertView) {
            super(convertView);

            txt_notification_title = (TextView) convertView.findViewById(R.id.txt_notification_title);
            txt_notification_discription = (TextView) convertView.findViewById(R.id.txt_notification_discription);
            txt_notification_time = (TextView) convertView.findViewById(R.id.txt_notification_time);

            ll_notification_row_parent  = (LinearLayout) convertView.findViewById(R.id.ll_notification_row_parent);
        }
    }

    public NotificationsAdapter(NotificationActivity ctx, ArrayList<NotificationData> NotificationList) {
        this.mNotificationList = NotificationList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_notification, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationData mNotification = mNotificationList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_notification_row_parent.setLayoutParams(params);

        holder.txt_notification_title.setText(mNotification.getTitle());

        holder.txt_notification_title.setTypeface( mContext.getSemiBoldFonts());
        holder.txt_notification_discription.setTypeface( mContext.getRegularFonts());
        holder.txt_notification_time.setTypeface( mContext.getRegularFonts());

        des = mNotification.getDescription();

        if(des.length() > 100){

            des  =  des.substring(0,100)+" ...";

            holder.txt_notification_discription.setText(des);
            holder.txt_notification_discription.setText(Html.fromHtml(des));
            holder.txt_notification_discription.setMovementMethod(LinkMovementMethod.getInstance());
        }else{
            holder.txt_notification_discription.setText(Html.fromHtml(des)); //Dont do any change
            holder.txt_notification_discription.setMovementMethod(LinkMovementMethod.getInstance());
        }

        try {
            Log.e("TimeFromModel", ""+mNotification.getCreateAt());

            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            Date date = mSimpleDateFormat.parse(mNotification.getCreateAt());

            long milliseconds = date.getTime();

//            Log.e("milliseconds", ""+milliseconds);
//            Log.e("AFTER_CONVERT",""+BaseActivity.getTimeAgo(milliseconds));

            holder.txt_notification_time.setText(BaseActivity.getTimeAgo(milliseconds));
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }
}