package com.kwapinspire.kwap.adapters;

import android.Manifest;
import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.BuildConfig;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.TopicsActivity;
import com.kwapinspire.kwap.model.TopicsData;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.ArrayList;

public class TopicsAdapter extends RecyclerView.Adapter<TopicsAdapter.MyViewHolder> {

    ArrayList<TopicsData> mTopicsList = new ArrayList<>();
    TopicsActivity mContext;
    ArrayList<Long> list = new ArrayList<>();

    private boolean isInternet;
    private android.app.DownloadManager downloadManager;
    private long refid;

    String mFilename;
    String CHANNEL_ID = "my_channel_01";

    Uri mDestinationUri;
    private ProgressDialog dialog;

    public TopicsAdapter(TopicsActivity ctx, ArrayList<TopicsData> TopicsList) {
        this.mTopicsList = TopicsList;
        this.mContext = ctx;

        isInternet = new InternetStatus().isInternetOn(ctx);
        downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);

        mContext.registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        createNotificationChannel();
        dialog = new ProgressDialog(mContext, R.style.MyAlertDialogStyle);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_topics, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TopicsData mTopics = mTopicsList.get(position);


        holder.text_topic_title.setText(mTopics.getTitle());
        holder.text_topic_dis.setText(mTopics.getDescription());
        Log.e("discription", ""+mTopics.getDescription());

        holder.text_topic_title.setTypeface(mContext.getSemiBoldFonts());
        holder.text_topic_dis.setTypeface(mContext.getLightFonts());
        holder.btn_download_topic.setTypeface(mContext.getRegularFonts());

        if (mTopics.getPdf().trim().equalsIgnoreCase("")) {
            holder.btn_download_topic.setVisibility(View.GONE);
        }

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_parent_topics.setLayoutParams(params);

        holder.btn_download_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("Path", "" + mTopics.getPdfName());

                if (isInternet) {

                    mFilename = mTopics.getPdfName();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                            && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        mContext.AskForPermission();
                    }
                    else
                    {
                        initForDownload(mTopics.getPdf(), mTopics.getPdfName());
                    }

                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Glide.with(mContext).load(mTopics.getImage()).error(R.drawable.ic_image_holder).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.topic_progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.topic_progress.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.img_topics);
    }

    @Override
    public int getItemCount() {
        return mTopicsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text_topic_title, text_topic_dis;
        ImageView img_topics;
        ProgressBar topic_progress;
        Button btn_download_topic;
        LinearLayout ll_row_parent_topics;

        public MyViewHolder(View convertView) {
            super(convertView);

            text_topic_title = (TextView) convertView.findViewById(R.id.text_topic_title);
            text_topic_dis = (TextView) convertView.findViewById(R.id.text_topic_dis);

            img_topics = (ImageView) convertView.findViewById(R.id.img_topics);
            topic_progress = (ProgressBar) convertView.findViewById(R.id.topic_progress);

            btn_download_topic = (Button) convertView.findViewById(R.id.btn_download_topic);

            ll_row_parent_topics = convertView.findViewById(R.id.ll_row_parent_topics);
//            mContext.overrideFonts(ll_row_parent_topics);
        }
    }

    private void initForDownload(String DownloadURL, String Filename) {

        dialog.setTitle("Presentation is downloading"); // Setting Title
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("please check your notifications");
        dialog.setCancelable(false);
        dialog.show();

        list.clear();
        Log.e("DownloadURL", "" + DownloadURL);
        Log.e("Filename", "" + Filename);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadURL));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle(Filename);
        request.setDescription("Downloading...");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/KWAP/" + Filename);


        Log.e("Filename", "" + Filename);
        refid = downloadManager.enqueue(request);

        list.add(refid);

    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            Log.e("IN", "" + referenceId);

            list.remove(referenceId);

            if (list.isEmpty()) {

                Log.e("INSIDE", "" + referenceId);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }


                File sharedFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "KWAP/" + mFilename);
                Intent mPdfIntent = new Intent(Intent.ACTION_VIEW);
                mPdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+ ".provider", sharedFile);
                mPdfIntent.setDataAndType(uri, "application/pdf");

//                PackageManager pm = mContext.getPackageManager();
//                if (intent.resolveActivity(pm) != null) {
//                    mContext.startActivity(intent);
//                }

                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                mContext,
                                0,
                                mPdfIntent,
                                PendingIntent.FLAG_CANCEL_CURRENT
                        );

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                        .setSmallIcon(R.drawable.myicon)
                        .setContentTitle("Download completed")
                        .setContentText("Tap to Open")
                        .setContentIntent(resultPendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);
// notificationId is a unique int for each notification that you must define
                notificationManager.notify(455, mBuilder.build());

                Toast.makeText(mContext, "Download completed", Toast.LENGTH_SHORT).show();
            }
        }
    };

    public void OpenPdfFile(){

        File sharedFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "KWAP/" + mFilename);
//        File sharedFile = new File(Environment.DIRECTORY_DOWNLOADS, "/KWAP/" + mFilename);

        Log.e("FileProvider path", "  "+sharedFile.getAbsolutePath());

       // create new Intent
        Intent intent = new Intent(Intent.ACTION_VIEW);

// set flag to give temporary permission to external app to use your FileProvider
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

// generate URI, I defined authority as the application ID in the Manifest, the last param is file I want to open
        Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+ ".provider", sharedFile);

        Log.e("FileProvider path", ""+uri.toString()+ "  "+sharedFile.getAbsolutePath());
// I am opening a PDF file so I give it a valid MIME type
        intent.setDataAndType(uri, "application/pdf");

// validate that the device can open your File!
        PackageManager pm = mContext.getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            mContext.startActivity(intent);
        }

    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = mContext.getString(R.string.app_name);
            String description = mContext.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = mContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}