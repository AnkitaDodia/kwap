package com.kwapinspire.kwap.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.SpeakersActivity;
import com.kwapinspire.kwap.model.SpeakersData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SpeakerAdapter extends RecyclerView.Adapter<SpeakerAdapter.MyViewHolder> {

    ArrayList<SpeakersData> mSpeakersList = new ArrayList<>();
    SpeakersActivity mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView text_speaker_name, text_speaker_dis;
        CircleImageView img_speaker;
        ProgressBar progress_speaker;
        LinearLayout speaker_list_main;

        public MyViewHolder(View convertView) {
            super(convertView);

            speaker_list_main = convertView.findViewById(R.id.speaker_list_main);
//            mContext.overrideFonts(speaker_list_main);

            text_speaker_name =  convertView.findViewById(R.id.text_speaker_name);
            text_speaker_dis = convertView.findViewById(R.id.text_speaker_dis);

            img_speaker = convertView.findViewById(R.id.img_speaker);
            progress_speaker = convertView.findViewById(R.id.progress_speaker);
        }
    }

    public SpeakerAdapter(SpeakersActivity ctx, ArrayList<SpeakersData> SpeakersList) {
        this.mSpeakersList = SpeakersList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_grid_speakers, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SpeakersData mSpeakers = mSpeakersList.get(position);

        holder.text_speaker_name.setText(mSpeakers.getName());
        holder.text_speaker_dis.setText(mSpeakers.getPosition());

        holder.text_speaker_name.setTypeface(mContext.getMediumFonts());
        holder.text_speaker_dis.setTypeface(mContext.getLightFonts());

        String imgUrl = mSpeakers.getImage();

        Log.e("imgUrl", "imgUrl = "+imgUrl);

        Glide.with(mContext).load(mSpeakers.getImage())
                .error(R.drawable.ic_logo_profile)
                .listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.progress_speaker.setVisibility(View.GONE);
                holder.img_speaker.setImageResource(R.drawable.ic_logo_profile);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.progress_speaker.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.img_speaker);
    }

    @Override
    public int getItemCount() {
        return mSpeakersList.size();
    }
}