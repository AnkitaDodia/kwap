package com.kwapinspire.kwap.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kwapinspire.kwap.AboutActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.model.OrganizerData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by My 7 on 18-May-18.
 */

public class OrganizerAdapter extends RecyclerView.Adapter<OrganizerAdapter.OrganizerViewHolder>
{
    AboutActivity mContext;

    ArrayList<OrganizerData> mList = new ArrayList<>();

    public class OrganizerViewHolder extends RecyclerView.ViewHolder {

        TextView text_organizer_title, text_organizer_dis;

        LinearLayout layout_organizer_main;

        ImageView image_organizer;

        ProgressBar progress_list_organizer;

        public OrganizerViewHolder(View convertView) {
            super(convertView);

            text_organizer_title = convertView.findViewById(R.id.text_organizer_title);
            text_organizer_dis = convertView.findViewById(R.id.text_organizer_dis);

            layout_organizer_main  = convertView.findViewById(R.id.layout_organizer_main);

            image_organizer = convertView.findViewById(R.id.image_organizer);

            progress_list_organizer = convertView.findViewById(R.id.progress_list_organizer);
        }
    }

    public OrganizerAdapter(AboutActivity ctx, ArrayList<OrganizerData> mData) {
        this.mList = mData;
        this.mContext = ctx;
    }

    @Override
    public OrganizerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_organizer, null);

        return new OrganizerAdapter.OrganizerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OrganizerViewHolder holder, int position) {
        OrganizerData data = mList.get(position);

        switch (position)
        {
            case 0:
                holder.text_organizer_title.setText("HOSTED BY:");
                Glide.with(mContext).load(R.drawable.hosted_by).into(holder.image_organizer);
                break;
            case 1:
                holder.text_organizer_title.setText("KNOWLEDGE ADVISOR:");
                Glide.with(mContext).load(R.drawable.advisor).into(holder.image_organizer);
                break;
        }
        holder.text_organizer_dis.setText(Html.fromHtml(data.getPageDescription()));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
