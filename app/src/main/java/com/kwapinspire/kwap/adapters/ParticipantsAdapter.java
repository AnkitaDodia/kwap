//package com.kwapinspire.kwap.adapters;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import com.kwapinspire.kwap.ParticipantsActivity;
//import com.kwapinspire.kwap.R;
//import com.kwapinspire.kwap.model.ParticipantsData;
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.resource.drawable.GlideDrawable;
//import com.bumptech.glide.request.RequestListener;
//import com.bumptech.glide.request.target.Target;
//import java.util.ArrayList;
//import java.util.Locale;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//
///**
// * Created by My 7 on 17-May-18.
// */
//
//public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ParticipantsHolder>
//{
//    ParticipantsActivity mContext;
//
//    ArrayList<ParticipantsData> mData = new ArrayList<>();
//    ArrayList<ParticipantsData> arraylist = new ArrayList<>();
//
//    public class ParticipantsHolder extends RecyclerView.ViewHolder
//    {
//        TextView text_participants_name,text_participants_position;
//
//        LinearLayout layout_participant_row_main;
//
//        CircleImageView img_participants;
//
//        ProgressBar progress_participants;
//
//        public ParticipantsHolder(View view) {
//            super(view);
//            text_participants_name = view.findViewById(R.id.text_participants_name);
//            text_participants_position = view.findViewById(R.id.text_participants_position);
//
//            img_participants = view.findViewById(R.id.img_participants);
//
//            progress_participants = view.findViewById(R.id.progress_participants);
//
//            layout_participant_row_main = view.findViewById(R.id.layout_participant_row_main);
//        }
//    }
//
//    public ParticipantsAdapter(ParticipantsActivity mContext, ArrayList<ParticipantsData> mList) {
//        this.mData = mList;
//        this.mContext = mContext;
//        this.arraylist.addAll(mData); //For filter
//    }
//
//    @Override
//    public ParticipantsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(mContext);
//        View itemView = inflater.inflate(R.layout.row_grid_participatnts, parent, false);
//        ParticipantsHolder Vh = new ParticipantsHolder(itemView);
//        return Vh;
//    }
//
//    @Override
//    public void onBindViewHolder(final ParticipantsHolder holder, int position) {
//        ParticipantsData data = mData.get(position);
//
//        holder.text_participants_name.setText(data.getName());
//        holder.text_participants_position.setText(data.getPosition());
//
//        holder.text_participants_name.setTypeface(mContext.getMediumFonts());
//        holder.text_participants_position.setTypeface(mContext.getLightFonts());
//
//        Glide.with(mContext).load(data.getProfileImage())
//                .error(R.drawable.ic_logo_profile)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        holder.progress_participants.setVisibility(View.GONE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        holder.progress_participants.setVisibility(View.GONE);
//                        return false;
//                    }
//                }).into(holder.img_participants);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mData.size();
//    }
//
//    // Filter Class
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        mData.clear();
//        if (charText.equals("notext")) {
//            mData.addAll(arraylist);
//        }
//        else
//        {
//            for (ParticipantsData wp : arraylist)
//            {
//                if (wp.getName().toLowerCase().contains(charText))
//                {
//                    mData.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//}
