package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.About;
import com.kwapinspire.kwap.model.OrganizerData;
import com.kwapinspire.kwap.model.Organizers;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import java.util.ArrayList;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by My 7 on 16-May-18.
 */

public class AboutActivity extends BaseActivity
{
    AboutActivity mContext;

    boolean isInternet;

    LinearLayout about_main, ll_about_back;

    TextView txt_toolbar_title, text_about, text_title_hosted, text_hosted, text_title_advisor, text_advisor;

    ProgressBar about_progress;

    ImageView image_about;

    ArrayList<OrganizerData> organizerData = new ArrayList<>();

    String text,about_image;

    WebView webview_about_details, webview_hosted_details, webview_advisor_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setFonts();
        setListener();
//        overrideFonts(about_main);

        if (isInternet) {
            sendContentRequest();
            sendOrganizerRequest();
        }
        else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        about_main = findViewById(R.id.about_main);
        ll_about_back = findViewById(R.id.ll_about_back);
        text_about = findViewById(R.id.text_about);
        image_about = findViewById(R.id.image_about);
        about_progress = findViewById(R.id.about_progress);

        text_advisor = findViewById(R.id.text_advisor);
        text_hosted = findViewById(R.id.text_hosted);

        webview_about_details = findViewById(R.id.webview_about_details);
        webview_hosted_details = findViewById(R.id.webview_hosted_details);
        webview_advisor_details = findViewById(R.id.webview_advisor_details);

    }

    private void setFonts(){

        text_title_advisor = findViewById(R.id.text_title_advisor);
        text_title_hosted = findViewById(R.id.text_title_hosted);

        txt_toolbar_title.setTypeface(getMediumFonts());
        text_about.setTypeface(getLightFonts());

        text_title_hosted.setTypeface(getMediumFonts());
        text_hosted.setTypeface(getLightFonts());

        text_title_advisor.setTypeface(getMediumFonts());
        text_advisor.setTypeface(getLightFonts());

    }

    private void setListener() {
        ll_about_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });
    }

    private void sendContentRequest(){
        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getAboutUs("page_about",new Callback<About>() {
            @Override
            public void success(About content, Response response) {

                if (response.getStatus() == 200) {
                    Log.e("ABOUT", "" + new Gson().toJson(content));

                    try {
                        String about = content.getPageDescription();
                        text = about;
//                        text ="<style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/Poppins-Light.ttf\")}body,* {font-family: MyFont; font-size: 13px;text-align: justify;}img{max-width:100%;height:auto; border-radius: 8px;}</style>";
//                        text = "<html><body style=\"color:#9e9e9e;font-family:file:///android_asset//Poppins-Light.ttf;font-size:16px;\"'background-color:transparent' >" + "<p align=\"justify\">" + about + "</p>" + "</body></html>";
//                        text = "<html><body style=\"font-family:file:///android_asset//Poppins-Light.ttf;\"'background-color:transparent' >" + about + "</p>" + "</body></html>";

                        about_image = content.getPageImage();

                        showWaitIndicator(false);

                        setAboutNDOrganizer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void sendOrganizerRequest()
    {
        if(mProgressDialogVisible())
        {
            showWaitIndicator(false);
            showWaitIndicator(true);
        }

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getOrganizers(new Callback<Organizers>() {
            @Override
            public void success(Organizers model, Response response) {

                if (response.getStatus() == 200) {
                    Log.e("ORGANIZER_RESPONSE", "" + new Gson().toJson(model));

                    try {
                        showWaitIndicator(false);

                        if(model.getStatus() == 1)
                        {
                            organizerData = model.getData();
                            Log.e("SIZE",""+organizerData.size());

                            setAboutNDOrganizer();
                        }
                        else
                        {
                            Toast.makeText(mContext,""+model.getMessage(),Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void setAboutNDOrganizer()
    {
        //Set about data
        text_about.setText(Html.fromHtml(text));

//        webview_about_details.loadData("<font>" + text + "</font>",
//                "text/html; charset=UTF-8", null);

        webview_about_details.loadDataWithBaseURL("file:///android_asset/",
                getStyledFont(text),
                "text/html; charset=UTF-8", null, "about:blank");

        Glide.with(mContext)
                .load(about_image)
                .error(R.drawable.ic_image_holder)
                .listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                about_progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                about_progress.setVisibility(View.GONE);
                return false;
            }
        })
        .into(image_about);

        if(organizerData.size() > 0)
        {
//            text_hosted.setText(Html.fromHtml(organizerData.get(0).getPageDescription()));
//            text_hosted.setMovementMethod(LinkMovementMethod.getInstance());
//
//            text_advisor.setText(Html.fromHtml(organizerData.get(1).getPageDescription()));
//            text_advisor.setMovementMethod(LinkMovementMethod.getInstance());

//            webview_hosted_details.loadData("<font>" + organizerData.get(0).getPageDescription() + "</font>",
//                    "text/html; charset=UTF-8", null);
//
//            webview_advisor_details.loadData("<font>" + organizerData.get(1).getPageDescription() + "</font>",
//                    "text/html; charset=UTF-8", null);

            webview_hosted_details.loadDataWithBaseURL("file:///android_asset/",
                    getStyledFont(organizerData.get(0).getPageDescription()),
                    "text/html; charset=UTF-8", null, "about:blank");

            webview_advisor_details.loadDataWithBaseURL("file:///android_asset/",
                    getStyledFont(organizerData.get(1).getPageDescription()),
                    "text/html; charset=UTF-8", null, "about:blank");
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
