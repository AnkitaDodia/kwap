package com.kwapinspire.kwap;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.NotificationData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class NotificationDetailsActivity extends BaseActivity {

    Context mContext;

    LinearLayout ll_parent_notification_details, ll_notification_details_back;

    TextView txt_toolbar_title, txt_notification_title, txt_notification_discription;//, txt_notification_link;

    ImageView img_notification;

    Button btn_notification_previous, btn_notification_next;

    ProgressBar notification_progress;
    WebView webview_notification_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);

        mContext = this;

        setView();
        setListners();
//        overrideFonts(ll_parent_notification_details);

        ShowNotificationDetails(BaseActivity.NotificationPosition);

        Log.e("ArraySize", "ArraySize" + BaseActivity.mNotificaionList.size());
        if (BaseActivity.mNotificaionList.size() == 1) {
            btn_notification_previous.setVisibility(View.INVISIBLE);
            btn_notification_next.setVisibility(View.INVISIBLE);
            Log.e("ArraySize", "ArraySize else if if");
        } else if (BaseActivity.NotificationPosition == BaseActivity.mNotificaionList.size() - 1) {
            btn_notification_next.setVisibility(View.INVISIBLE);
            Log.e("ArraySize", "ArraySize if");
        } else if (BaseActivity.NotificationPosition == 0) {
            btn_notification_previous.setVisibility(View.INVISIBLE);
            Log.e("ArraySize", "ArraySize else if");
        }


    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        ll_parent_notification_details = findViewById(R.id.ll_parent_notification_details);
        ll_notification_details_back = findViewById(R.id.ll_notification_details_back);

        txt_notification_title = findViewById(R.id.txt_notification_title);
        txt_notification_discription = findViewById(R.id.txt_notification_discription);
//        txt_notification_link = findViewById(R.id.txt_notification_link);

        img_notification = findViewById(R.id.img_notification);
        notification_progress = findViewById(R.id.notification_progress);

        btn_notification_previous = findViewById(R.id.btn_notification_previous);
        btn_notification_next = findViewById(R.id.btn_notification_next);

        webview_notification_details = findViewById(R.id.webview_notification_details);

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_notification_title.setTypeface(getSemiBoldFonts());

        btn_notification_previous.setTypeface(getRegularFonts());
        btn_notification_next.setTypeface(getRegularFonts());
    }

    private void setListners() {

        ll_notification_details_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_notification_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("NotificationPosition", "position : " + BaseActivity.NotificationPosition);
                Log.d("NotificationPosition", "size : " + BaseActivity.mNotificaionList.size());


                if (BaseActivity.NotificationPosition < BaseActivity.mNotificaionList.size()) {

                    btn_notification_next.setVisibility(View.VISIBLE);

                    BaseActivity.NotificationPosition--;

                    Log.d("NotificationPosition", "position : " + BaseActivity.NotificationPosition);
                    ShowNotificationDetails(BaseActivity.NotificationPosition);

                    if (BaseActivity.NotificationPosition == 0) {
                        btn_notification_previous.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        btn_notification_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("NotificationPosition", "position : " + BaseActivity.NotificationPosition);
                Log.d("NotificationPosition", "size : " + BaseActivity.mNotificaionList.size());

                if (BaseActivity.NotificationPosition < BaseActivity.mNotificaionList.size() - 1) {
                    btn_notification_previous.setVisibility(View.VISIBLE);

                    BaseActivity.NotificationPosition++;

                    Log.d("NotificationPosition", "position : " + BaseActivity.NotificationPosition);
                    ShowNotificationDetails(BaseActivity.NotificationPosition);


                    if (BaseActivity.NotificationPosition == BaseActivity.mNotificaionList.size() - 1) {
                        btn_notification_next.setVisibility(View.INVISIBLE);

                    }
                }
            }
        });
    }

    private void ShowNotificationDetails(int postion) {

//        BaseActivity.mTodayNotificaionList

        NotificationData mNotification = BaseActivity.mNotificaionList.get(postion);

        txt_notification_title.setText(mNotification.getTitle());
        txt_notification_discription.setText(Html.fromHtml(mNotification.getDescription()));
        txt_notification_discription.setMovementMethod(LinkMovementMethod.getInstance());


        webview_notification_details.loadData("<font>" + mNotification.getDescription() + "</font>",
                "text/html; charset=UTF-8", null);

        Log.e("getImage()", "" + mNotification.getImage());

        if (!mNotification.getImage().equals("")) {
            Glide.with(mContext)
                    .load(mNotification.getImage())
                    .error(R.drawable.ic_image_holder)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            notification_progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            notification_progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(img_notification);
        } else {
            notification_progress.setVisibility(View.GONE);
            img_notification.setImageResource(R.drawable.image_placeholder);
        }

    }
}
