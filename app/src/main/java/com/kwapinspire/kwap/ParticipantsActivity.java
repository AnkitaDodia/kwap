package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Participants;
import com.kwapinspire.kwap.model.ParticipantsData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.ItemDecorationAlbumColumns;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ParticipantsActivity extends BaseActivity {

    ParticipantsActivity mContext;

    boolean isInternet;

    LinearLayout ll_parent_participants, ll_participants_back,error_layout_participants;

    RecyclerView rv_grid_participants;

    EditText search;

    ArrayList<ParticipantsData> participantsData = new ArrayList<>();

    ParticipantsAdapter adapter;

    String error = "No record found!!";

    TextView txt_toolbar_title, text_error_participants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participants);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListners();
//        overrideFonts(ll_parent_participants);

        if (isInternet) {
            sendParticipantsRequest();
        }
        else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        ll_parent_participants = findViewById(R.id.ll_parent_participants);
        ll_participants_back =  findViewById(R.id.ll_participants_back);
        error_layout_participants = findViewById(R.id.error_layout_participants);

        text_error_participants = findViewById(R.id.text_error_participants);

        search = findViewById(R.id.search);

        rv_grid_participants = findViewById(R.id.rv_grid_participants);
        rv_grid_participants.addItemDecoration(new ItemDecorationAlbumColumns(5,2));

//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
//        rv_grid_participants.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        txt_toolbar_title.setTypeface(getMediumFonts());
        search.setTypeface(getRegularFonts());
        text_error_participants.setTypeface(getRegularFonts());
    }

    private void setListners(){
        ll_participants_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub

                String text = search.getText().toString();
                if(text.length() > 2)
                {
                    adapter.getFilter().filter(text);
                }
                else if(text.length() == 0)
                {
                    BaseActivity.tempParticipantsData.clear();
                    participantsData = BaseActivity.participantsData;
                    setParticipantsAdapter(participantsData);
                }
            }
        });

        rv_grid_participants.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_grid_participants, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

//                BaseActivity.ParticipantsPosition = position;
//                BaseActivity.participantId = participantsData.get(position).getUserId();
//                Toast.makeText(mContext,"From: "+BaseActivity.ParticipantsPosition,Toast.LENGTH_LONG).show();
                BaseActivity.mParticipantsData = new ParticipantsData();
                if( BaseActivity.tempParticipantsData.size() > 0)
                {
                    BaseActivity.mParticipantsData =  BaseActivity.tempParticipantsData.get(position);
                }
                else
                {
                    BaseActivity.mParticipantsData =  participantsData.get(position);
                }

                Intent it = new Intent(mContext, ParticipantsDetailsActivity.class);
                mContext.startActivity(it);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendParticipantsRequest()
    {
        showWaitIndicator(true);

        String userId = getUserId();
        Log.e("userId", "" + userId);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getParticipants(userId,new Callback<Participants>() {
            @Override
            public void success(Participants data, Response response) {

                if (response.getStatus() == 200) {
                    try {
                        Log.e("PARTICIPANTS_RESPONSE", "" + new Gson().toJson(data));

                        if(data.getStatus()== 1){
                            participantsData = data.getData();

                            showWaitIndicator(false);

                            BaseActivity.participantsData = participantsData;

                        }else{
                            error = data.getMessage();
                        }

                        setParticipantsAdapter(participantsData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void setParticipantsAdapter(ArrayList<ParticipantsData> data) {
        if(data.size() > 0)
        {
            error_layout_participants.setVisibility(View.GONE);
            rv_grid_participants.setVisibility(View.VISIBLE);

            adapter = new ParticipantsAdapter(mContext,data);
            rv_grid_participants.setLayoutManager(new GridLayoutManager(mContext, 2));
            rv_grid_participants.setAdapter(adapter);
        }
        else
        {
            text_error_participants.setText(error);

            rv_grid_participants.setVisibility(View.GONE);
            error_layout_participants.setVisibility(View.VISIBLE);
        }
    }

    public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ParticipantsHolder> implements Filterable
    {
        ParticipantsActivity mContext;

        ArrayList<ParticipantsData> mData = new ArrayList<>();
        ArrayList<ParticipantsData> arraylist = new ArrayList<>();

        public class ParticipantsHolder extends RecyclerView.ViewHolder
        {
            TextView text_participants_name,text_participants_position;

            LinearLayout layout_participant_row_main;

            CircleImageView img_participants;

            ProgressBar progress_participants;

            public ParticipantsHolder(View view) {
                super(view);
                text_participants_name = view.findViewById(R.id.text_participants_name);
                text_participants_position = view.findViewById(R.id.text_participants_position);

                img_participants = view.findViewById(R.id.img_participants);

                progress_participants = view.findViewById(R.id.progress_participants);

                layout_participant_row_main = view.findViewById(R.id.layout_participant_row_main);
            }
        }

        public ParticipantsAdapter(ParticipantsActivity mContext, ArrayList<ParticipantsData> mList) {
            this.mData = mList;
            this.mContext = mContext;
            this.arraylist = mList; //For filter
        }

        @Override
        public ParticipantsAdapter.ParticipantsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View itemView = inflater.inflate(R.layout.row_grid_participatnts, parent, false);
            ParticipantsAdapter.ParticipantsHolder Vh = new ParticipantsAdapter.ParticipantsHolder(itemView);
            return Vh;
        }

        @Override
        public void onBindViewHolder(final ParticipantsAdapter.ParticipantsHolder holder, int position) {
            ParticipantsData data = mData.get(position);

            holder.text_participants_name.setText(data.getName());
            holder.text_participants_position.setText(data.getPosition());

            holder.text_participants_name.setTypeface(mContext.getMediumFonts());
            holder.text_participants_position.setTypeface(mContext.getLightFonts());

            Glide.with(mContext).load(data.getProfileImage())
                    .error(R.drawable.ic_logo_profile)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.progress_participants.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.progress_participants.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.img_participants);
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        mData = BaseActivity.participantsData;
                        Log.e("CALLL","IFFFFF");
                    } else {
                        Log.e("CALLL","ELSE");
                        ArrayList<ParticipantsData> filteredList = new ArrayList<>();
                        for (ParticipantsData row : arraylist) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getPhone().contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        mData = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mData;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mData = (ArrayList<ParticipantsData>) filterResults.values;
                    Log.e("Size",""+mData.size());
                    BaseActivity.tempParticipantsData = mData;
                    if(mData.size() > 0)
                    {
                        notifyDataSetChanged();
                    }
                    else
                    {
                        setParticipantsAdapter(mData);
                    }
                }
            };
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
