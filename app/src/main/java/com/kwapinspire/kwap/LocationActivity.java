package com.kwapinspire.kwap;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Location;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LocationActivity extends BaseActivity {

    Context mContext;
    LinearLayout ll_parent_location, ll_location_back;
    ImageView img_location;
    TextView txt_toolbar_title, txt_title_direction, txt_location_discription;
    Button btn_waze, btn_google_map;

    ProgressBar location_progress;

    boolean isInternet;
    public String latitude, longitude;
    WebView webview_location_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        mContext = this;

        setView();
        setFonts();
        setListener();


        isInternet = new InternetStatus().isInternetOn(mContext);

        if (isInternet) {
            sendLocationRequest();
        }
        else {
            Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        txt_title_direction = findViewById(R.id.txt_title_direction);

        ll_parent_location = findViewById(R.id.ll_parent_location);
        ll_location_back = findViewById(R.id.ll_location_back);

        img_location = findViewById(R.id.img_location);

        txt_location_discription = findViewById(R.id.txt_location_discription);

        btn_waze = findViewById(R.id.btn_waze);
        btn_google_map = findViewById(R.id.btn_google_map);

        location_progress = findViewById(R.id.location_progress);
        webview_location_details = findViewById(R.id.webview_location_details);
    }

    private void setFonts(){
        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_title_direction.setTypeface(getSemiBoldFonts());

        txt_location_discription.setTypeface(getLightFonts());

        btn_waze.setTypeface(getRegularFonts());
        btn_google_map.setTypeface(getRegularFonts());
    }

    private void setListener() {
        btn_waze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://waze.com/ul/hw283fw0hp")));
            }
        });

        ll_location_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        btn_google_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                "geo:"+latitude+","+longitude+""
//                Uri gmmIntentUri = Uri.parse("geo:"+latitude+","+longitude+"");
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                if (mapIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(mapIntent);
//                }

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/place/Grand+Hyatt+Kuala+Lumpur/@3.1537061,101.7101625,17z/data=!4m10!1m2!2m1!1sgrand+hyatt+kl!3m6!1s0x31cc37d463fa3ec9:0x1df56b16c8bfdbe8!5m1!1s2018-06-09!8m2!3d3.1536674!4d101.7122952")));
            }
        });
    }

    private void sendLocationRequest(){
        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getLocation(new Callback<Location>() {
            @Override
            public void success(Location content, Response response) {

                if (response.getStatus() == 200) {
                    Log.e("ABOUT", "" + new Gson().toJson(content));

                    try {
                        setLogin(1);
                        setData(content);

                        showWaitIndicator(false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    public void setData(Location content){

//        String about = content.getDescription();
//        String text = "<html><body style=\"color:#9e9e9e;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:16px;\"'background-color:transparent' >" + "<p align=\"justify\">" + about + "</p>" + "</body></html>";

//        txt_location_discription.setText(Html.fromHtml(content.getDescription()));
//        txt_location_discription.setMovementMethod(LinkMovementMethod.getInstance());

        latitude = content.getLatitude();
        longitude = content.getLongitude();


        webview_location_details.loadDataWithBaseURL("file:///android_asset/",
                getStyledFont(content.getDescription()),
                "text/html; charset=UTF-8", null, "about:blank");

        Glide.with(mContext).load(content.getImage()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                location_progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                location_progress.setVisibility(View.GONE);
                return false;
            }
        }).into(img_location);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
