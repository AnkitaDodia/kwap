package com.kwapinspire.kwap.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.kwapinspire.kwap.model.Profile;

import java.io.Serializable;

public class SettingsPreferences implements Serializable {

	private static final String PREFS_NAME = "KwapDB";

	private static final String DEFAULT_VAL = null;

	private static final String Key_user_id = "userId";
	private static final String Key_user_name = "username";
	private static final String Key_name= "name";
	private static final String Key_user_email = "email";
	private static final String Key_user_phone = "phone";
	private static final String Key_user_company = "company";
	private static final String Key_user_position = "position";
	private static final String Key_qr_code= "qr_code";
	private static final String Key_country= "country";
	private static final String Key_profile_image= "profile_image";

	public static void clearDB(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();
		dbEditor.clear();
		dbEditor.commit();

	}

	public static void storeConsumer(Context context, Profile user) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();

		dbEditor.putString(Key_user_id, user.getUserId());
		dbEditor.putString(Key_user_name, user.getUsername());
		dbEditor.putString(Key_name, user.getName());
		dbEditor.putString(Key_user_email, user.getEmail());
		dbEditor.putString(Key_user_phone, user.getPhone());
		dbEditor.putString(Key_user_company, user.getCompany());
		dbEditor.putString(Key_user_position, user.getPosition());
		dbEditor.putString(Key_qr_code, user.getQrCode());
		dbEditor.putString(Key_country, user.getCountry());
		dbEditor.putString(Key_profile_image,user.getProfileImage());

		dbEditor.commit();
	}

	public static Profile getConsumer(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		Profile user = new Profile();

		user.setCompany(prefs.getString(Key_user_company,DEFAULT_VAL));
		user.setCountry(prefs.getString(Key_country,DEFAULT_VAL));
		user.setEmail(prefs.getString(Key_user_email,DEFAULT_VAL));
		user.setName(prefs.getString(Key_name,DEFAULT_VAL));
		user.setPhone(prefs.getString(Key_user_phone,DEFAULT_VAL));
		user.setPosition(prefs.getString(Key_user_position,DEFAULT_VAL));
		user.setQrCode(prefs.getString(Key_qr_code,DEFAULT_VAL));
		user.setUserId(prefs.getString(Key_user_id,DEFAULT_VAL));
		user.setUsername(prefs.getString(Key_user_name,DEFAULT_VAL));
		user.setProfileImage(prefs.getString(Key_profile_image,DEFAULT_VAL));

		return user;
	}
}
