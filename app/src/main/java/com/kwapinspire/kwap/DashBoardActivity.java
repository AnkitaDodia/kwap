package com.kwapinspire.kwap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.customviews.SquareRelativeLayout;
import com.kwapinspire.kwap.utility.NotificationUtils;

public class DashBoardActivity extends BaseActivity
{
    DashBoardActivity mContext;

    RecyclerView rv_grid_dashboard;
    TextView txt_link1, txt_link2;
    LinearLayout ll_parent_dashboard,layout_link_1,layout_link_2;

    boolean doubleBackToExitPressedOnce = false;

    NotificationUtils notificationUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mContext = this;

        notificationUtils = new NotificationUtils(getApplicationContext());

        setView();
        setListener();
        setData();
    }

    private void setView() {
        rv_grid_dashboard = findViewById(R.id.rv_grid_dashboard);
        ll_parent_dashboard = findViewById(R.id.ll_parent_dashboard);
        layout_link_1 =  findViewById(R.id.layout_link_1);
        layout_link_2 =  findViewById(R.id.layout_link_2);

        txt_link1 =  findViewById(R.id.txt_link1);
        txt_link2 =  findViewById(R.id.txt_link2);

        txt_link1.setTypeface(getMediumFonts());
        txt_link2.setTypeface(getMediumFonts());
    }

    private void setListener()
    {
        layout_link_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webViewLink = "http://www.kwap.gov.my";
                startActivity(new Intent(mContext, WebViewActivity.class));
                finish();
            }
        });

        layout_link_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webViewLink = "http://www.kwapinspire.com.my/";
                startActivity(new Intent(mContext, WebViewActivity.class));
                finish();
            }
        });
    }

    private void setData()
    {
        Log.e("FLAG",""+notificationUtils.getIsNew());

        Bitmap[] logos = new Bitmap[15];
        logos[0] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_qr_code);
        logos[1] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification);
        logos[2] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_about);
        logos[3] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_topics);
        logos[4] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_agenda);
        logos[5] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_speakers);
        logos[6] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_participants);
        logos[7] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_location);
        logos[8] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_socialmedia);
        logos[9] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_news);
        logos[10] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_sponsers);
        logos[11] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_contactus);
        logos[12] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
        logos[13] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_qa_poll);
        logos[14] = BitmapFactory.decodeResource(getResources(), R.drawable.ic_eventsurvey);

        MyAdapter adapter = new MyAdapter(getResources().getStringArray(R.array.menu_list), logos);
        rv_grid_dashboard.setLayoutManager(new GridLayoutManager(mContext, 3));
        rv_grid_dashboard.setAdapter(adapter);
    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

        String[] mMenuList;
        Bitmap[] logoList;

        public MyAdapter(String[] mMenuList, Bitmap[] logoList) {
            this.mMenuList = mMenuList;
            this.logoList = logoList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_grid_dashboard, parent, false);
            MyViewHolder viewHolder = new MyViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.logo.setImageBitmap(logoList[position]);
            holder.ll_parent_gridrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (position) {
                        case 0:
                            startActivity(new Intent(mContext, QRCodeActivity.class));
                            finish();
                            break;
                        case 1:
                            startActivity(new Intent(mContext, NotificationActivity.class));
                            finish();
                            break;
                        case 2:
                            startActivity(new Intent(mContext, AboutActivity.class));
                            finish();
                            break;
                        case 3:
                            startActivity(new Intent(mContext, TopicsActivity.class));
                            finish();
                            break;
                        case 4:
                            startActivity(new Intent(mContext, AgendaActivity.class));
                            finish();
                            break;
                        case 5:
                            startActivity(new Intent(mContext, SpeakersActivity.class));
                            finish();
                            break;
                        case 6:
                            startActivity(new Intent(mContext, ParticipantsActivity.class));
                            finish();
                            break;
                        case 7:
                            startActivity(new Intent(mContext, LocationActivity.class));
                            finish();
                            break;
                        case 8:
                            startActivity(new Intent(mContext, SocialMediaActivity.class));
                            finish();
                            break;
                        case 9:
                            startActivity(new Intent(mContext, NewsActivity.class));
                            finish();
                            break;
                        case 10:
                            startActivity(new Intent(mContext, SponsorsActivity.class));
                            finish();
                            break;
                        case 11:
                            startActivity(new Intent(mContext, ContactUsActivity.class));
                            finish();
                            break;
                        case 12:
                            startActivity(new Intent(mContext, ProfileActivity.class));
                            finish();
                            break;
                        case 13:
                            webViewLink = "http://polls.kwapinspire.com.my/";
                            startActivity(new Intent(mContext, WebViewActivity.class));
                            finish();
                            break;
                        case 14:
                            webViewLink = "https://www.kwapinspire.com.my/survey";
                            startActivity(new Intent(mContext, WebViewActivity.class));
                            finish();
//                            startActivity(new Intent(mContext, EventSurveyActivity.class));
//                            finish();
                            break;
                    }
                }
            });
            holder.name.setText(mMenuList[position]);
            holder.name.setTypeface(getLightFonts());

            switch (position) {
                case 0:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_dark);
                    break;
                case 1:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_light);

                    if(notificationUtils.getIsNew())
                    {
//                        holder.logo.setImageResource(0);
//                        holder.logo.setImageResource(R.drawable.ic_notification_active);
                        holder.logo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification_active));
                        Log.e("IF","CALLLLLLLL");
                    }
                    else
                    {
                        Log.e("ELSE","CALLLLLLLL");
//                        holder.logo.setImageResource(0);
//                        holder.logo.setImageResource(R.drawable.ic_notification);
                        holder.logo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification));
                    }
                    break;
                case 2:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_dark);
                    break;
                case 3:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_medium);
                    break;
                case 4:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_dark);
                    break;
                case 5:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_medium);
                    break;
                case 6:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_dark);
                    break;
                case 7:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_medium);
                    break;
                case 8:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_light);
                    break;
                case 9:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_light);
                    break;
                case 10:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_dark);
                    break;
                case 11:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_medium);
                case 12:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_medium);
                    break;
                case 13:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_light);
                    break;
                case 14:
                    holder.ll_parent_gridrow.setBackgroundResource(R.drawable.menu_selector_dark);
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return mMenuList.length;
        }
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView logo;
        public TextView name, txt_count;
        public SquareRelativeLayout ll_parent_gridrow;

        public MyViewHolder(View itemView) {
            super(itemView);
            logo = (ImageView) itemView.findViewById(R.id.ivLogo);
            name = (TextView) itemView.findViewById(R.id.tvCompany);
            txt_count = (TextView) itemView.findViewById(R.id.txt_count);
            ll_parent_gridrow = (SquareRelativeLayout) itemView.findViewById(R.id.ll_parent_gridrow);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(mContext , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}