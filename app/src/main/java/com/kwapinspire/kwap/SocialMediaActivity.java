package com.kwapinspire.kwap;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kwapinspire.kwap.common.BaseActivity;

public class SocialMediaActivity extends BaseActivity {

    Context mContext;
    TextView txt_toolbar_title, txt_media_header, txt_youtube, txt_instagram;
    LinearLayout ll_socialmedia_back, ll_youtube, ll_instagram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);

        mContext = this;
        setView();
        SetFonts();
        SetListner();
    }

    private void setView(){

        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);

        txt_media_header  = findViewById(R.id.txt_media_header);

        txt_youtube = findViewById(R.id.txt_youtube);
        txt_instagram = findViewById(R.id.txt_instagram);

        ll_socialmedia_back = findViewById(R.id.ll_socialmedia_back);
        ll_youtube = findViewById(R.id.ll_youtube);
        ll_instagram = findViewById(R.id.ll_instagram);
    }

    private void SetFonts(){

        txt_toolbar_title.setTypeface(getMediumFonts());

        txt_media_header.setTypeface(getSemiBoldFonts());

        txt_youtube.setTypeface(getRegularFonts());
        txt_instagram.setTypeface(getRegularFonts());
    }

    private void SetListner(){

        ll_socialmedia_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        ll_youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCdp4EmbXOynr7VoKFd9b5dw?view_as=subscriber")));
            }
        });

        ll_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/kwapinspire/")));
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }


}
