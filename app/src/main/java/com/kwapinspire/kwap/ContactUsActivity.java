package com.kwapinspire.kwap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.ContactUs;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 15-May-18.
 */

public class ContactUsActivity extends BaseActivity
{
    ContactUsActivity mContext;

    boolean isInternet;

    LinearLayout contact_main, ll_contactus_back;

    Button btn_send_contact;

    TextView txt_contact_header, txt_toolbar_title, text_contact_number,txt_contact_details, text_contact_mail;

    ImageButton image_button_insta,image_button_you_tube;

    EditText edt_name,edt_youremail,edt_msg;

    String name,email,msg;

    protected static final int REQUEST_CALL_PHONE_ACCESS_PERMISSION = 101;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contact_us);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setFonts();
//        overrideFonts(contact_main);
        setListener();
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        txt_contact_header = findViewById(R.id.txt_contact_header);
        txt_contact_details = findViewById(R.id.txt_contact_details);

        contact_main = findViewById(R.id.contact_main);

        btn_send_contact = findViewById(R.id.btn_send_contact);

        text_contact_number = findViewById(R.id.text_contact_number);
        text_contact_mail = findViewById(R.id.text_contact_mail);

        ll_contactus_back = findViewById(R.id.ll_contactus_back);

        image_button_insta = findViewById(R.id.image_button_insta);
        image_button_you_tube = findViewById(R.id.image_button_you_tube);

        edt_name = findViewById(R.id.edt_name);
        edt_youremail = findViewById(R.id.edt_youremail);
        edt_msg = findViewById(R.id.edt_msg);


    }

    private void setFonts(){

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_contact_header.setTypeface(getMediumFonts());

        txt_contact_details.setTypeface(getSemiBoldFonts());
        text_contact_number.setTypeface(getRegularFonts());
        text_contact_mail.setTypeface(getRegularFonts());

        edt_name.setTypeface(getLightFonts());
        edt_youremail.setTypeface(getLightFonts());
        edt_msg.setTypeface(getLightFonts());

    }

    private void setListener() {
        ll_contactus_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        btn_send_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_name.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please fill up all required fields!!",Toast.LENGTH_LONG).show();
                }
                else if(edt_youremail.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please fill up all required fields!!",Toast.LENGTH_LONG).show();
                }
                else if(!isValidEmail(edt_youremail.getText().toString()))
                {
                    Toast.makeText(mContext,"Please enter valid email address!!",Toast.LENGTH_LONG).show();
                }
                else if(edt_msg.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please fill up all required fields!!",Toast.LENGTH_LONG).show();
                }
                else
                {
                    name = edt_name.getText().toString();
                    email = edt_youremail.getText().toString();
                    msg = edt_msg.getText().toString();

                    if (isInternet) {
                        sendContactRequest();
                    }
                    else {
                        Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        text_contact_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    AskForPermission();
                }
                else
                {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:60321748000"));
                    startActivity(callIntent);
                }
            }
        });

        text_contact_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@kwapinspire.com.my"});

                if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                    mContext.startActivity(intent);
                }
            }
        });

        image_button_insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/kwapinspire/")));
            }
        });

        image_button_you_tube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCdp4EmbXOynr7VoKFd9b5dw?view_as=subscriber")));
            }
        });
    }

    private void sendContactRequest()
    {
        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.sendContactUsDetails(name,email,msg,new Callback<ContactUs>() {
            @Override
            public void success(ContactUs model, Response response) {

                if (response.getStatus() == 200) {
                    Log.e("CONTACT", "" + new Gson().toJson(model));
                    showWaitIndicator(false);

                    try {
                        if(model.getStatus() == 1)
                        {
                            edt_name.setText("");
                            edt_youremail.setText("");
                            edt_msg.setText("");

                            startActivity(new Intent(mContext,ContactUsThankUActivity.class));
                        }
                        else
                        {
                            Toast.makeText(mContext,""+model.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }

    private void AskForPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.CALL_PHONE,
                    getString(R.string.permission_call_phone_rationale),
                    REQUEST_CALL_PHONE_ACCESS_PERMISSION);
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CALL_PHONE_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
