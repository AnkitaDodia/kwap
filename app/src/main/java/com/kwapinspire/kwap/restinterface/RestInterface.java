package com.kwapinspire.kwap.restinterface;

import com.kwapinspire.kwap.model.About;
import com.kwapinspire.kwap.model.AgendaDetails;
import com.kwapinspire.kwap.model.Agendas;
import com.kwapinspire.kwap.model.ContactUs;
import com.kwapinspire.kwap.model.Location;
import com.kwapinspire.kwap.model.Login;
import com.kwapinspire.kwap.model.Notification;
import com.kwapinspire.kwap.model.Organizers;
import com.kwapinspire.kwap.model.Participants;
import com.kwapinspire.kwap.model.Profile;
import com.kwapinspire.kwap.model.QRCode;
import com.kwapinspire.kwap.model.Speakers;
import com.kwapinspire.kwap.model.SpeakersDetails;
import com.kwapinspire.kwap.model.Sponsors;
import com.kwapinspire.kwap.model.Survey;
import com.kwapinspire.kwap.model.SurveyAnswers;
import com.kwapinspire.kwap.model.Topics;
import com.kwapinspire.kwap.model.UpdateProfile;
import com.kwapinspire.kwap.model.FCM;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by codfidea on 6/30/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "http://app.kwapinspire.com.my";

    String LOGIN = "/APIUser/apiLogin";
    String GET_PROFILE = "/APIUser/apiSingleUser";
    String PARTICIPANTS = "/APIUser/apiAllUser";
    String SPEAKERS = "/Api/apiAllSpeaker";
    String SPEAKERSDETAILS = "/Api/apiSingleSpeaker";
    String TOPICS = "/Api/apiAllTopic";
    String TOPICSDETAILS = "/Api/apiSingleTopic";
    String SPONSORS = "/Api/apiAllSponsor";
    String NOTIFICATION = "/Api/apiAllNotification";
    String ABOUTUS = "/APIPages/apiSinglePage";
    String ORGANIZERS = "/APIPages/apiAllOrganiserPages";
    String QRCODE = "/APIUser/GetUserQR";
    String AGENDA = "/Api/apiAllAgenda";
    String AGENDADETAILS = "/Api/apiSingleAgenda";
    String CONTACT = "/Api/apiContactUs";
    String LOCATION = "/APIPages/apiLocationPage";
    String UPDATE_PROFILE = "/APIUser/apiEdit";
    String GET_SURVEY = "/Api/apiAllSurvey";
    String SURVEYANSWERS = "/Api/apiSurveyAnswers";
    String FCMTOKEN = "/Api/apiNfcToken";


    //LogIn
    @FormUrlEncoded
    @POST(LOGIN)
    public void sendLoginRequest(@Field("email") String email, @Field("password") String password
            , @Field("device_id") String device_id,Callback<Login> callBack);

    //Get Profile
    @FormUrlEncoded
    @POST(GET_PROFILE)
    public void GetProfileRequest(@Field("userId") String userId, Callback<Profile> callBack);

    // Participants
    @FormUrlEncoded
    @POST(PARTICIPANTS)
    public void getParticipants(@Field("userId") String userId,Callback<Participants> callback);

    //Speakers
    @GET(SPEAKERS)
    public void getSpeakersRequest(Callback<Speakers> callback);

    //Speakers Details
    @FormUrlEncoded
    @POST(SPEAKERSDETAILS)
    public void getSpeakersDetailsRequest(@Field("speaker_id") String speaker_id, Callback<SpeakersDetails> callback);

    //Topics
    @GET(TOPICS)
    public void getTopicsRequest(Callback<Topics> callback);

    //TopicsDetails
    @FormUrlEncoded
    @POST(TOPICSDETAILS)
    public void getTopicsDetailsRequest(Callback<Topics> callback);

    // Sponsors
    @GET(SPONSORS)
    public void getSponsors(Callback<Sponsors> callback);

    // Notification
    @FormUrlEncoded
    @POST(NOTIFICATION)
    public void getNotifications(@Field("day") String day, Callback<Notification> callback);

    // About
    @FormUrlEncoded
    @POST(ABOUTUS)
    public void getAboutUs(@Field("page_name") String page_name, Callback<About> callback);

    // Organizers
    @GET(ORGANIZERS)
    public void getOrganizers(Callback<Organizers> callback);

    // Agenda
    @GET(AGENDA)
    public void getAgenda(Callback<Agendas> callback);

    // QRCode
    @FormUrlEncoded
    @POST(QRCODE)
    public void getQRCode(@Field("userId") String userId, Callback<QRCode> callback);

    // Agenda Details
    @FormUrlEncoded
    @POST(AGENDADETAILS)
    public void getAgendaDetails(@Field("agenda_id") String agenda_id, Callback<AgendaDetails> callback);

    // Location
    @GET(LOCATION)
    public void getLocation(Callback<Location> callback);

    // ContactUs
    @FormUrlEncoded
    @POST(CONTACT)
    public void sendContactUsDetails(@Field("name") String name,@Field("email") String email,
                                     @Field("message") String message,Callback<ContactUs> callback);

    @Multipart
    @POST(UPDATE_PROFILE)
    public void updateProfileRequest(@Part("image") TypedFile image,@Part("userId") String userId
                                     ,@Part("name") String name,@Part("email") String email
                                     ,@Part("phone") String phone,@Part("company") String company,
                                     @Part("position") String position,@Part("country") String country
                                     ,@Part("password") String password,Callback<UpdateProfile> callback);

    //Speakers
    @FormUrlEncoded
    @POST(GET_SURVEY)
    public void getSurveyRequest(@Field("userId") String userId,Callback<Survey> callback);

    // Send Servey answers
    @FormUrlEncoded
    @POST(SURVEYANSWERS)
    public void sendSurveyAnswers(@Field("userId") String userId, @Field("answers") String answers, Callback<SurveyAnswers> callback);

    // Send Servey answers
    @FormUrlEncoded
    @POST(FCMTOKEN)
    public void sendFCMToken(@Field("nfc_token") String nfc_token, Callback<FCM> callback);
}
