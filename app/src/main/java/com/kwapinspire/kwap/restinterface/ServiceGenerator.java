package com.kwapinspire.kwap.restinterface;

import com.squareup.okhttp.OkHttpClient;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by ankita on 05-May-16.
 */
public class ServiceGenerator
{
    private static RestAdapter.Builder builder = new RestAdapter.Builder()
            .setEndpoint(RestInterface.API_BASE_URL)
            .setClient(new OkClient(new OkHttpClient()));

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }
}