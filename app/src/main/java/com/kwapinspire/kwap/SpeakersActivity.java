package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.adapters.SpeakerAdapter;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Speakers;
import com.kwapinspire.kwap.model.SpeakersData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.ItemDecorationAlbumColumns;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 16-May-18.
 */

public class SpeakersActivity extends BaseActivity
{
    SpeakersActivity mContext;

    boolean isInternet;

    LinearLayout speakers_main, ll_speaker_back,error_layout_speaker;

    RecyclerView rv_grid_speaker;

    TextView txt_toolbar_title, text_error_speaker;

    private ArrayList<SpeakersData> SpeakersList = new ArrayList<>();

    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakers);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListener();
//        overrideFonts(speakers_main);

        if (isInternet) {
            getTopicListRequest();
        } else {
            Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        speakers_main = findViewById(R.id.speakers_main);
        ll_speaker_back = findViewById(R.id.ll_speaker_back);
        error_layout_speaker = findViewById(R.id.error_layout_speaker);

        text_error_speaker = findViewById(R.id.text_error_speaker);

        rv_grid_speaker = findViewById(R.id.rv_grid_speaker);
        rv_grid_speaker.addItemDecoration(new ItemDecorationAlbumColumns(5,2));
//                getResources().getDimensionPixelSize(R.dimen.photos_list_spacing),
//                getResources().getInteger(R.integer.photo_list_preview_columns)));

        txt_toolbar_title.setTypeface(getMediumFonts());
        text_error_speaker.setTypeface(getRegularFonts());
    }

    private void setListener() {
        ll_speaker_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        rv_grid_speaker.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_grid_speaker, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.SpeakerId = SpeakersList.get(position).getSpeakerId();
                Intent it = new Intent(mContext, SpeakersDetailsActivity.class);
                mContext.startActivity(it);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

   private void getTopicListRequest()
    {
        try{
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getSpeakersRequest(new Callback<Speakers>() {
                @Override
                public void success(Speakers model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SPEKERS_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                SpeakersList = model.getData();
                            }
                            else
                            {
                                msg = model.getMessage();
                            }
                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListData()
    {
        if(SpeakersList.size() > 0)
        {
            error_layout_speaker.setVisibility(View.GONE);
            rv_grid_speaker.setVisibility(View.VISIBLE);

            SpeakerAdapter mSpeakerAdapter = new SpeakerAdapter(mContext, SpeakersList);

            rv_grid_speaker.setLayoutManager(new GridLayoutManager(mContext, 2));
            rv_grid_speaker.setAdapter(mSpeakerAdapter);
        }
        else
        {
            text_error_speaker.setText(msg);
            error_layout_speaker.setVisibility(View.VISIBLE);
            rv_grid_speaker.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
