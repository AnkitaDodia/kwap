package com.kwapinspire.kwap.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */

import android.content.SharedPreferences;
import android.util.Log;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.FCM;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CustomFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = CustomFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        BaseActivity.refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token_Value: " + BaseActivity.refreshedToken);

        saveFCMID(BaseActivity.refreshedToken);

        sendFCMTokenRequest();
    }

    public void saveFCMID(String id) {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("FCM_ID", id);
        spe.commit();
    }

    private void sendFCMTokenRequest() {
        try {
//            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendFCMToken(BaseActivity.refreshedToken, new Callback<FCM>() {
                @Override
                public void success(FCM model, Response response) {
//                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("FCM_TOKEN_RESPONSE", "" + new Gson().toJson(model));



                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

//                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}