package com.kwapinspire.kwap;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.utility.NotificationUtils;

public class SplashActivity extends BaseActivity {


    private static int SPLASH_TIME_OUT = 3000;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();

            String title= extras.getString("title");
            String message = extras.getString("message");
            String clickAction= extras.getString("click_action");
            String fromTopic = extras.getString("fromTopic");

//            Log.e("Bundle","extras  : "+someData+"  "+someData2);

            Log.e("Bundle",""+title);
            Log.e("Bundle",""+message);
            Log.e("Bundle",""+clickAction);
            Log.e("Bundle",""+fromTopic);

            if(title != null){

                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.saveIsNew(true);
            }

        }


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(getFreshInstall() && getLogin() == 1)
                {
                    Intent i = new Intent(mContext, UpdateProfileActivity.class);
                    startActivity(i);
                    finish();
                }
                else if(getLogin() == 1 && !getFreshInstall())
                {
                    Intent i = new Intent(mContext, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Intent i = new Intent(mContext, SignInActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);
    }
}
