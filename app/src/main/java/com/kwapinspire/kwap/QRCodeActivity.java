package com.kwapinspire.kwap;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.QRCode;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class QRCodeActivity extends BaseActivity {

    Context mContext;
    LinearLayout ll_parent_qrcode, ll_qrcode_back;
    ImageView img_qr_code;
    TextView txt_toolbar_title, txt_qr_discription, txt_title_fullname, txt_title_email, txt_title_phonenumber, txt_title_company, txt_fullname, txt_email, txt_phonenumber, txt_company;

    boolean isInternet;
    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setFonts();
        setListners();
//        overrideFonts(ll_parent_qrcode);

        if (isInternet) {
            getQRCodeRequest();
        } else {
            Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

    }

    private void setView() {
        ll_parent_qrcode = (LinearLayout) findViewById(R.id.ll_parent_qrcode);

        ll_qrcode_back = (LinearLayout) findViewById(R.id.ll_qrcode_back);

        img_qr_code = (ImageView) findViewById(R.id.img_qr_code);

        txt_fullname = (TextView) findViewById(R.id.txt_fullname);
        txt_email = (TextView) findViewById(R.id.txt_email);
        txt_phonenumber = (TextView) findViewById(R.id.txt_phonenumber);
        txt_company = (TextView) findViewById(R.id.txt_company);

        txt_toolbar_title = (TextView) findViewById(R.id.txt_toolbar_title);

        txt_qr_discription = (TextView) findViewById(R.id.txt_qr_discription);
        txt_title_fullname= (TextView) findViewById(R.id.txt_title_fullname);
        txt_title_email= (TextView) findViewById(R.id.txt_title_email);
        txt_title_phonenumber= (TextView) findViewById(R.id.txt_title_phonenumber);
        txt_title_company= (TextView) findViewById(R.id.txt_title_company);

    }

    private void setFonts(){

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_qr_discription.setTypeface(getLightFonts());

        txt_title_fullname.setTypeface(getSemiBoldFonts());
        txt_title_email.setTypeface(getSemiBoldFonts());
        txt_title_phonenumber.setTypeface(getSemiBoldFonts());
        txt_title_company.setTypeface(getSemiBoldFonts());

        txt_fullname.setTypeface(getLightFonts());
        txt_email.setTypeface(getLightFonts());
        txt_phonenumber.setTypeface(getLightFonts());
        txt_company.setTypeface(getLightFonts());
    }
    private void setListners() {

        ll_qrcode_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });
    }

    private void getQRCodeRequest() {
        try {
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getQRCode(getUserId(), new Callback<QRCode>() {
                @Override
                public void success(QRCode model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("QRCODE_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus() == 1) {


                            }

                            msg = model.getMessage();

                            setListData(model);

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setListData(QRCode model) {
        txt_fullname.setText(model.getName());
        txt_email.setText(model.getEmail());
        txt_phonenumber.setText(model.getPhone());
        txt_company.setText(model.getCompany());


        Glide.with(mContext)
                .load(model.getQrCode())
                .error(R.drawable.ic_image_holder)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        holder.progress_sponsors.setVisibility(View.GONE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        holder.progress_sponsors.setVisibility(View.GONE);
//                        return false;
//                    }
//                })
                .into(img_qr_code);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
