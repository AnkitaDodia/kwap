package com.kwapinspire.kwap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Login;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignInActivity extends BaseActivity {

    SignInActivity mContext;

    boolean isInternet;

    EditText edt_youremail, edt_password;

    TextView txt_toolbar_title, txt_forgot_password;

    Button btn_signin;

    LinearLayout ll_parent_signin;
    protected static final int REQUEST_PHONE_STATE_READ_ACCESS_PERMISSION = 101;
//    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        SetListener();
        SetFonts();
//        overrideFonts(ll_parent_signin);
        ReadPhoneState();
    }

    private void setView() {
        ll_parent_signin = findViewById(R.id.ll_parent_signin);

        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);

        edt_youremail = findViewById(R.id.edt_youremail);
        edt_password = findViewById(R.id.edt_password);

        btn_signin = findViewById(R.id.btn_signin);

    }

    private void SetFonts(){

        txt_toolbar_title.setTypeface(getLightFonts());

        txt_forgot_password.setTypeface(getRegularFonts());
        edt_youremail.setTypeface(getLightFonts());
        edt_password.setTypeface(getLightFonts());

        btn_signin.setTypeface(getLightFonts());

    }

    private void SetListener() {

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_youremail.getText().toString().length() == 0 || edt_password.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter email address and password!!", Toast.LENGTH_LONG).show();
                } else if (!isValidEmail(edt_youremail.getText().toString())) {
                    Toast.makeText(mContext, "Please enter valid email address!!", Toast.LENGTH_LONG).show();
                } else {
                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
                View mView = getLayoutInflater().inflate(R.layout.dialog_forgot_password, null);
                final CircularImageView img_close = (CircularImageView) mView.findViewById(R.id.img_close);
                final TextView txt_dailog_mail = (TextView) mView.findViewById(R.id.txt_dailog_mail);
                final TextView txt_please_contact = (TextView) mView.findViewById(R.id.txt_please_contact);
                final TextView txt_resetpassword = (TextView) mView.findViewById(R.id.txt_resetpassword);
                final ConstraintLayout ll_parent_forgotpassword =  mView.findViewById(R.id.ll_parent_forgotpassword);

                txt_dailog_mail.setTypeface(getSemiBoldFonts());
                txt_resetpassword.setTypeface(getSemiBoldFonts());
                txt_please_contact.setTypeface(getSemiBoldFonts());

//                mContext.overrideFonts(ll_parent_forgotpassword);

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
                img_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();

                    }
                });

                txt_dailog_mail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(android.net.Uri.parse("mailto:")); // only email apps should handle this
                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@kwapinspire.com.my"});

                        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                            mContext.startActivity(intent);
                        }

                    }
                });

            }
        });
    }

    private void sendLoginRequest() {
        try {
            String email = edt_youremail.getText().toString();
            String password = edt_password.getText().toString();
            String device_id = getIMEINumber();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendLoginRequest(email, password, device_id, new Callback<Login>() {
                @Override
                public void success(Login model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("LOGIN_RESPONSE", "" + new Gson().toJson(model));

                            //if successfully login then status 1 else 0
                            if (model.getStatus() == 1) {
                                setUserId(model.getUserId());
                                setLogin(1);

                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();

                                if (getFreshInstall()) {
                                    Intent i = new Intent(mContext, UpdateProfileActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Intent i = new Intent(mContext, DashBoardActivity.class);
                                    startActivity(i);
                                    finish();
                                }

                            } else {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void ReadPhoneState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_PHONE_STATE,
                    getString(R.string.permission_call_state_rationale),
                    REQUEST_PHONE_STATE_READ_ACCESS_PERMISSION);
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_STATE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    getIMEINumber();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            /*Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);*/
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


}
