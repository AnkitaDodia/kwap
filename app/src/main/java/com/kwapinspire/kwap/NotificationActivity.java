package com.kwapinspire.kwap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.fragments.NotificationPreviousFragment;
import com.kwapinspire.kwap.fragments.NotificationTodayFragment;
import com.kwapinspire.kwap.utility.NotificationUtils;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseActivity {

    Context mContext;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    LinearLayout ll_parent_notification, ll_notification_back;
    TextView txt_toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        mContext = this;

        setView();
        setListners();
//        overrideFonts(ll_parent_notification);
        setupViewPager(viewPager);

        //For show alert on notification
        NotificationUtils notificationUtils = new NotificationUtils(mContext);
        notificationUtils.saveIsNew(false);
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        ll_parent_notification = findViewById(R.id.ll_parent_notification);
        viewPager = findViewById(R.id.viewpager);

        ll_notification_back  = findViewById(R.id.ll_notification_back);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0cb4cf"));
//        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#434343"), Color.parseColor("#434343"));

        txt_toolbar_title.setTypeface(getRegularFonts());
    }

    private void setListners(){

        ll_notification_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NotificationTodayFragment(), "Today");
        adapter.addFragment(new NotificationPreviousFragment(), "Previous");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
