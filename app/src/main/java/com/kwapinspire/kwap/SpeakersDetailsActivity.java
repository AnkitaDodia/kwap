package com.kwapinspire.kwap;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.SpeakersDetails;
import com.kwapinspire.kwap.model.SpeakersDetailsData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 16-May-18.
 */

public class SpeakersDetailsActivity extends BaseActivity
{
    SpeakersDetailsActivity mContext;

    boolean isInternet;

    LinearLayout ll_parent_speakersdetails, ll_speakersdetails_back;

    CircularImageView img_speaker_detail;

    ProgressBar progress_speaker_detail;

    TextView txt_toolbar_title, txt_title_fullname, txt_title_company, txt_title_position, txt_fullname, txt_company, txt_position, txt_discription;

    private ArrayList<SpeakersDetailsData> SpeakersList = new ArrayList<>();

    String msg;
    WebView webview_speaker_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakersdetails);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListener();

        if (isInternet) {
            getSpeakersDetailsRequest();
        } else {
            Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        ll_parent_speakersdetails = findViewById(R.id.ll_parent_speakersdetails);
        ll_speakersdetails_back = findViewById(R.id.ll_speakersdetails_back);

        img_speaker_detail = findViewById(R.id.img_speaker_detail);
        progress_speaker_detail = findViewById(R.id.progress_speaker_detail);

        txt_title_fullname= findViewById(R.id.txt_title_fullname);
        txt_title_company= findViewById(R.id.txt_title_company);
        txt_title_position= findViewById(R.id.txt_title_position);

        txt_fullname = findViewById(R.id.txt_fullname);
        txt_company = findViewById(R.id.txt_company);
        txt_position = findViewById(R.id.txt_position);
        txt_discription = findViewById(R.id.txt_discription);

        webview_speaker_details = findViewById(R.id.webview_speaker_details);

        txt_toolbar_title.setTypeface(getMediumFonts());

        txt_title_fullname.setTypeface(getLightFonts());
        txt_title_company.setTypeface(getLightFonts());
        txt_title_position.setTypeface(getLightFonts());

        txt_fullname.setTypeface(getSemiBoldFonts());
        txt_company.setTypeface(getSemiBoldFonts());
        txt_position.setTypeface(getSemiBoldFonts());

    }

    private void setListener() {
        ll_speakersdetails_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getSpeakersDetailsRequest()
    {
        try{
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getSpeakersDetailsRequest(BaseActivity.SpeakerId, new Callback<SpeakersDetails>() {
                @Override
                public void success(SpeakersDetails model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SPEKERS_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                SpeakersList = model.getData();
                                setListData();
                            }
                            else
                            {
                                Toast.makeText(mContext,""+model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListData()
    {
        txt_fullname.setText(SpeakersList.get(0).getName());
        txt_company.setText(SpeakersList.get(0).getCompany());
        txt_position.setText(SpeakersList.get(0).getPosition());

        txt_discription.setText(Html.fromHtml(SpeakersList.get(0).getDescription()));
        txt_discription.setMovementMethod(LinkMovementMethod.getInstance());


//        webview_speaker_details.loadData("<font>" + SpeakersList.get(0).getDescription() + "</font>",
//                "text/html; charset=UTF-8", null);

        webview_speaker_details.loadDataWithBaseURL("file:///android_asset/",
                getStyledFont(SpeakersList.get(0).getDescription()),
                "text/html; charset=UTF-8", null, "about:blank");

        Glide.with(mContext).load(SpeakersList.get(0).getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progress_speaker_detail.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progress_speaker_detail.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(img_speaker_detail);
    }
}
