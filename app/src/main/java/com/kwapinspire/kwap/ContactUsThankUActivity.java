package com.kwapinspire.kwap;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kwapinspire.kwap.common.BaseActivity;

/**
 * Created by My 7 on 24-May-18.
 */

public class ContactUsThankUActivity extends BaseActivity
{
    ContactUsThankUActivity mContext;

    LinearLayout contact_thank_u_main,ll_contactus_thanku_back;
    TextView txt_toolbar_title, txt_thankyou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us_thank_u);

        mContext = this;

        setView();
        setClickListener();
//        overrideFonts(contact_thank_u_main);
    }

    private void setView()
    {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        contact_thank_u_main = findViewById(R.id.contact_thank_u_main);
        ll_contactus_thanku_back = findViewById(R.id.ll_contactus_thanku_back);

        txt_thankyou = findViewById(R.id.txt_thankyou);

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_thankyou.setTypeface(getMediumFonts());
    }

    private void setClickListener()
    {
        ll_contactus_thanku_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
