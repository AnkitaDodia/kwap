package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.ParticipantsData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mikhaellopez.circularimageview.CircularImageView;

public class ParticipantsDetailsActivity extends BaseActivity {

    ParticipantsDetailsActivity mContext;

    LinearLayout ll_parent_profilepage, ll_profilepage_back;

    Button btn_add_contact;

    CircularImageView img_profile_page;

    TextView txt_toolbar_title, txt_title_fullname, txt_fullname, txt_title_company, txt_company, txt_title_position, txt_position, txt_title_email, txt_email, txt_title_phone, txt_phone, txt_title_country, txt_country;

    ProgressBar progress_profile_page;

//    ParticipantsData mParticipantsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participants_details);

        mContext = this;

        setView();
        setFonts();
        setListners();
//        overrideFonts(ll_parent_profilepage);

//        Toast.makeText(mContext,"To: "+BaseActivity.ParticipantsPosition,Toast.LENGTH_LONG).show();
//        mParticipantsData = BaseActivity.participantsData.get(BaseActivity.ParticipantsPosition);

        SetData(BaseActivity.mParticipantsData);
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);

        ll_parent_profilepage = findViewById(R.id.ll_parent_profilepage);
        ll_profilepage_back = findViewById(R.id.ll_profilepage_back);

        img_profile_page = findViewById(R.id.img_profile_page);

        progress_profile_page = findViewById(R.id.progress_profile_page);

        btn_add_contact = findViewById(R.id.btn_add_contact);

        txt_title_fullname = findViewById(R.id.txt_title_fullname);
        txt_title_company = findViewById(R.id.txt_title_company);
        txt_title_position = findViewById(R.id.txt_title_position);
        txt_title_email = findViewById(R.id.txt_title_email);
        txt_title_phone = findViewById(R.id.txt_title_phone);
        txt_title_country = findViewById(R.id.txt_title_country);
    }

    private void setFonts(){

        txt_fullname = findViewById(R.id.txt_fullname);
        txt_company = findViewById(R.id.txt_company);
        txt_position = findViewById(R.id.txt_position);
        txt_email = findViewById(R.id.txt_email);
        txt_phone = findViewById(R.id.txt_phone);
        txt_country = findViewById(R.id.txt_country);

        txt_toolbar_title.setTypeface(getMediumFonts());

        txt_title_fullname.setTypeface(getLightFonts());
        txt_title_company.setTypeface(getLightFonts());
        txt_title_position.setTypeface(getLightFonts());
        txt_title_email.setTypeface(getLightFonts());
        txt_title_phone.setTypeface(getLightFonts());
        txt_title_country.setTypeface(getLightFonts());

        txt_fullname.setTypeface(getSemiBoldFonts());
        txt_company.setTypeface(getSemiBoldFonts());
        txt_position.setTypeface(getSemiBoldFonts());
        txt_email.setTypeface(getSemiBoldFonts());
        txt_phone.setTypeface(getSemiBoldFonts());
        txt_country.setTypeface(getSemiBoldFonts());

        btn_add_contact.setTypeface(getRegularFonts());
    }

    private void setListners() {

        ll_profilepage_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddContact();
            }
        });
    }

    private void SetData(ParticipantsData mParticipantsData) {
        txt_fullname.setText(mParticipantsData.getName());
        txt_company.setText(mParticipantsData.getCompany());
        txt_position.setText(mParticipantsData.getPosition());

        txt_email.setText(mParticipantsData.getEmail());
        txt_phone.setText(mParticipantsData.getPhone());
        txt_country.setText(mParticipantsData.getCountry());

        Glide.with(mContext).load(mParticipantsData.getProfileImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progress_profile_page.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progress_profile_page.setVisibility(View.GONE);
                        return false;
                    }
                })
                .error(R.drawable.ic_logo_profile)
                .into(img_profile_page);
    }

    private void AddContact() {

        // Creates a new Intent to insert a contact
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
// Sets the MIME type to match the Contacts Provider
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        // Inserts an email address
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, BaseActivity.mParticipantsData.getEmail())
        .putExtra(ContactsContract.Intents.Insert.NAME, BaseActivity.mParticipantsData.getName())
                .putExtra(ContactsContract.Intents.Insert.COMPANY, BaseActivity.mParticipantsData.getCompany())
                .putExtra(ContactsContract.Intents.Insert.JOB_TITLE,BaseActivity.mParticipantsData.getPosition())
/*
 * In this example, sets the email type to be a work email.
 * You can set other email types as necessary.
 */
                .putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
// Inserts a phone number
                .putExtra(ContactsContract.Intents.Insert.PHONE, BaseActivity.mParticipantsData.getPhone())
/*
 * In this example, sets the phone type to be a work phone.
 * You can set other phone types as necessary.
 */
                .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);

        startActivity(intent);
    }
}
