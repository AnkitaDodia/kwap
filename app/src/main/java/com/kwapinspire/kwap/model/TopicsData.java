package com.kwapinspire.kwap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicsData {
@SerializedName("topic_id")
@Expose
private String topicId;
@SerializedName("title")
@Expose
private String title;
@SerializedName("description")
@Expose
private String description;
@SerializedName("image")
@Expose
private String image;
@SerializedName("pdf")
@Expose
private String pdf;
@SerializedName("pdf_name")
@Expose
private String pdfName;

public String getTopicId() {
return topicId;
}

public void setTopicId(String topicId) {
this.topicId = topicId;
}

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public String getPdf() {
return pdf;
}

public void setPdf(String pdf) {
this.pdf = pdf;
}

public String getPdfName() {
return pdfName;
}

public void setPdfName(String pdfName) {
this.pdfName = pdfName;
}
}
