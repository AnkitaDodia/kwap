package com.kwapinspire.kwap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by My 7 on 19-May-18.
 */

public class AgendaData
{
    @SerializedName("day1")
    @Expose
    private ArrayList<Day> day1 = null;
    @SerializedName("day2")
    @Expose
    private ArrayList<Day> day2 = null;

    public ArrayList<Day> getDay1() {
        return day1;
    }

    public void setDay1(ArrayList<Day> day1) {
        this.day1 = day1;
    }

    public ArrayList<Day> getDay2() {
        return day2;
    }

    public void setDay2(ArrayList<Day> day2) {
        this.day2 = day2;
    }
}
