package com.kwapinspire.kwap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by My 7 on 18-May-18.
 */

public class OrganizerData
{
    @SerializedName("page_id")
    @Expose
    private String pageId;
    @SerializedName("page_title")
    @Expose
    private String pageTitle;
    @SerializedName("page_name")
    @Expose
    private String pageName;
    @SerializedName("page_description")
    @Expose
    private String pageDescription;
    @SerializedName("page_image")
    @Expose
    private String pageImage;

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageDescription() {
        return pageDescription;
    }

    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    public String getPageImage() {
        return pageImage;
    }

    public void setPageImage(String pageImage) {
        this.pageImage = pageImage;
    }
}
