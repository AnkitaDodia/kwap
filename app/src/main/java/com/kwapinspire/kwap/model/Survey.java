package com.kwapinspire.kwap.model;

/**
 * Created by My 7 on 21-May-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Survey {
    @SerializedName("data")
    @Expose
    private ArrayList<SurveyData> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public ArrayList<SurveyData> getData() {
        return data;
    }

    public void setData(ArrayList<SurveyData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}