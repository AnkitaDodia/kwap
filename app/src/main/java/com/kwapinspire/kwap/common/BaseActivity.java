package com.kwapinspire.kwap.common;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.model.Day;
import com.kwapinspire.kwap.model.NotificationData;
import com.kwapinspire.kwap.model.ParticipantsData;
import com.kwapinspire.kwap.model.SponsorsData;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class BaseActivity extends AppCompatActivity
{
    private Typeface font;

    private static final String TAG = "BaseActivity";

    public static String SpeakerId,AgendaId,refreshedToken,webViewLink;
    public static int NotificationPosition;

    ProgressDialog mProgressDialog;

    public static ParticipantsData mParticipantsData;

    public static ArrayList<ParticipantsData> participantsData = new ArrayList<>();
    public static ArrayList<ParticipantsData> tempParticipantsData = new ArrayList<>();
    public static ArrayList<SponsorsData> sponsorsData = new ArrayList<>();
    public static ArrayList<Day> mDay1List = new ArrayList<>();
    public static ArrayList<Day> mDay2List = new ArrayList<>();

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEK_MILLIS = 7 * DAY_MILLIS;

    public static ArrayList<NotificationData> mNotificaionList = new ArrayList<>();

    // Code for get and set login
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login",i);
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }
    // Code for get and set login

    public void setUserId(String id)
    {
        SharedPreferences sp = getSharedPreferences("USER_ID",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("UserId",id);
        spe.apply();
    }

    public String getUserId()
    {
        SharedPreferences sp = getSharedPreferences("USER_ID",MODE_PRIVATE);
        String i = sp.getString("UserId","");
        return i;
    }

    public void saveFreshInstall(boolean flag)
    {
        SharedPreferences sp = getSharedPreferences("FRESH_INSTALL",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("INSTALL",flag);
        spe.apply();
        spe.commit();
    }

    public boolean getFreshInstall()
    {
        SharedPreferences sp = getSharedPreferences("FRESH_INSTALL",MODE_PRIVATE);
        boolean flag = sp.getBoolean("INSTALL",true);
        return flag;
    }

    public Typeface getTypeFace()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
        return font;
    }

    public Typeface getBoldFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Bold.ttf");
        return font;
    }

    public Typeface getSemiBoldFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-SemiBold.ttf");
        return font;
    }

    public Typeface getMediumFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
        return font;
    }

    public Typeface getRegularFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Regular.ttf");
        return font;
    }

    public Typeface getLightFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Light.ttf");
        return font;
    }

    public static String getStyledFont(String html) {
        boolean addBodyStart = !html.toLowerCase().contains("<body>");
        boolean addBodyEnd = !html.toLowerCase().contains("</body");
        return "<style type=\"text/css\">@font-face {font-family: CustomFont;" +
                "src: url(\"file:///android_asset/fonts/Poppins-Light.ttf\")}" +
                "body {font-family: CustomFont;font-size: small;}</style>" +
                (addBodyStart ? "<body>" : "") + html + (addBodyEnd ? "</body>" : "");
    }

  public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public boolean mProgressDialogVisible()
    {
        boolean flag;
        if(mProgressDialog.isShowing())
        {
            flag = true;
        }
        else
        {
            flag = false;
        }
        return flag;
    }

    public void showWaitIndicator(boolean state) {
        showWaitIndicator(state, "");
    }

    public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getTimeAgo(long Time) throws ParseException
    {
        long time = Time;

        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();

        Log.e("time",""+time);
        Log.e("now",""+now);

        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        Log.e("diff",""+diff);
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "1 minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 120 * MINUTE_MILLIS) {
            return "1 hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "1 day ago";
        } else if (diff < 7 * DAY_MILLIS) {
            return diff / DAY_MILLIS + " days ago";
        } else if (diff < 2 * WEEK_MILLIS) {
            return "1 week ago";
        } else {
            return diff / WEEK_MILLIS + " weeks ago";
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public String getFCMID() {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        String flag = sp.getString("FCM_ID", "");
        return flag;
    }



    public String getIMEINumber() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String IMEINumber = mngr.getDeviceId();
        return IMEINumber;
    }



    /**
     * Requests given permission.
     * If the permission has been denied previously, a Dialog will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    protected void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(BaseActivity.this,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }
    /**
     * This method shows dialog with given title & message.
     * Also there is an option to pass onClickListener for positive & negative button.
     *
     * @param title                         - dialog title
     * @param message                       - dialog message
     * @param onPositiveButtonClickListener - listener for positive button
     * @param positiveText                  - positive button text
     * @param onNegativeButtonClickListener - listener for negative button
     * @param negativeText                  - negative button text
     */
    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        AlertDialog mAlertDialog = builder.show();
    }
}
