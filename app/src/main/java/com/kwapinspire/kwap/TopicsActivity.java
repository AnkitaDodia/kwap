package com.kwapinspire.kwap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.adapters.TopicsAdapter;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Topics;
import com.kwapinspire.kwap.model.TopicsData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.SpacesItemDecoration;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 16-May-18.
 */

public class TopicsActivity extends BaseActivity
{
    TopicsActivity mContext;

    boolean isInternet;

    LinearLayout topics_main, ll_topics_back,error_layout_topics;

    RecyclerView rv_list_topic;

    TextView txt_toolbar_title, text_error_topics;

    private ArrayList<TopicsData> TopicsList = new ArrayList<>();

    String msg;
    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListener();
//        overrideFonts(topics_main);


        if (isInternet) {
            getTopicListRequest();
        } else {
            Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        topics_main = findViewById(R.id.topics_main);
        ll_topics_back = findViewById(R.id.ll_topics_back);
        error_layout_topics = findViewById(R.id.error_layout_topics);

        text_error_topics = findViewById(R.id.text_error_topics);

        rv_list_topic = findViewById(R.id.rv_list_topic);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rv_list_topic.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        txt_toolbar_title.setTypeface(getMediumFonts());
    }

    private void setListener() {
        ll_topics_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });
    }

    private void getTopicListRequest()
    {
        try{
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getTopicsRequest(new Callback<Topics>() {
                @Override
                public void success(Topics model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("TOPICS_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                TopicsList = model.getData();
                            }
                            else
                            {
                                msg = model.getMessage();
                            }

                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListData()
    {
        if(TopicsList.size() > 0)
        {
            error_layout_topics.setVisibility(View.GONE);
            rv_list_topic.setVisibility(View.VISIBLE);

            TopicsAdapter mTopicsAdapter = new TopicsAdapter(mContext, TopicsList);
            rv_list_topic.setLayoutManager(new LinearLayoutManager(mContext));
            rv_list_topic.getLayoutManager().setMeasurementCacheEnabled(false);
            rv_list_topic.setAdapter(mTopicsAdapter);
        }
        else
        {
            error_layout_topics.setVisibility(View.VISIBLE);
            rv_list_topic.setVisibility(View.GONE);
            text_error_topics.setText(msg);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }

    public void AskForPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_WRITE_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
