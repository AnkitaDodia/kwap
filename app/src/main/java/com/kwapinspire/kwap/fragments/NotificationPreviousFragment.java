package com.kwapinspire.kwap.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.NotificationActivity;
import com.kwapinspire.kwap.NotificationDetailsActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.adapters.NotificationsAdapter;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.Notification;
import com.kwapinspire.kwap.model.NotificationData;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.kwapinspire.kwap.utility.SpacesItemDecoration;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.widget.LinearLayout.VERTICAL;

public class NotificationPreviousFragment  extends Fragment
{
    boolean isInternet;

    ArrayList<NotificationData> mNotificationList = new ArrayList<>();

    NotificationActivity mContext;

    RecyclerView rv_notification;

    LinearLayout error_layout;

    TextView text_error;

    String msg;

    public NotificationPreviousFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification_today, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mContext = (NotificationActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView(view);


        if (isInternet) {
            getNotificationListRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void inItView(View view) {
        rv_notification = (RecyclerView) view.findViewById(R.id.rv_notification);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rv_notification.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        error_layout = view.findViewById(R.id.error_layout);

        text_error = view.findViewById(R.id.text_error);

        rv_notification.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_notification, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.mNotificaionList = mNotificationList;
                BaseActivity.NotificationPosition = position;

                Intent it = new Intent(mContext, NotificationDetailsActivity.class);
                mContext.startActivity(it);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        text_error.setTypeface(mContext.getRegularFonts());
    }

    private void setListData()
    {
        if(mNotificationList.size() > 0)
        {
            error_layout.setVisibility(View.GONE);
            rv_notification.setVisibility(View.VISIBLE);

            NotificationsAdapter mNotificationsAdapter = new NotificationsAdapter(mContext, mNotificationList);
            rv_notification.setLayoutManager(new LinearLayoutManager(mContext));
            rv_notification.getLayoutManager().setMeasurementCacheEnabled(false);
            DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
            rv_notification.addItemDecoration(itemDecor);
            rv_notification.setAdapter(mNotificationsAdapter);
        }
        else
        {
            error_layout.setVisibility(View.VISIBLE);
            rv_notification.setVisibility(View.GONE);
            text_error.setText(msg);
        }
    }

    private void getNotificationListRequest() {

        String DAY = "previous";

        try {
            if(mContext.mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getNotifications(DAY, new Callback<Notification>() {
                @Override
                public void success(Notification model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("NOTIFICATION_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                mNotificationList = model.getData();
                            }
                            else
                            {
                                msg = model.getMessage();
                            }
                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}