package com.kwapinspire.kwap.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kwapinspire.kwap.AgendaActivity;
import com.kwapinspire.kwap.R;
import com.kwapinspire.kwap.adapters.Day1AgendaAdapter;
import com.kwapinspire.kwap.common.BaseActivity;

/**
 * Created by My 7 on 19-May-18.
 */

public class Day1AgendaFragment extends Fragment
{
    AgendaActivity mContext;

    RecyclerView rv_agenda;

    LinearLayout error_layout_agenda;

    TextView text_error_agenda;


    public Day1AgendaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agenda, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mContext = (AgendaActivity) getActivity();
        inItView(view);
        setListData();
    }

    private void inItView(View view)
    {
        rv_agenda = view.findViewById(R.id.rv_agenda);

//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
//        rv_agenda.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        error_layout_agenda = view.findViewById(R.id.error_layout_agenda);

        text_error_agenda = view.findViewById(R.id.text_error_agenda);

        text_error_agenda.setTypeface(mContext.getRegularFonts());
    }

    private void setListData()
    {
        if(BaseActivity.mDay1List.size() > 0)
        {
            error_layout_agenda.setVisibility(View.GONE);
            rv_agenda.setVisibility(View.VISIBLE);

            Day1AgendaAdapter agendaAdapter = new Day1AgendaAdapter(mContext, BaseActivity.mDay1List);
            rv_agenda.setLayoutManager(new LinearLayoutManager(mContext));
            rv_agenda.getLayoutManager().setMeasurementCacheEnabled(false);
            rv_agenda.setAdapter(agendaAdapter);
        }
        else
        {
            error_layout_agenda.setVisibility(View.VISIBLE);
            rv_agenda.setVisibility(View.GONE);
            text_error_agenda.setText("No Agenda!!");
        }
    }
}
