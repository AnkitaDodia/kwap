package com.kwapinspire.kwap;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kwapinspire.kwap.common.BaseActivity;

public class NewsActivity extends BaseActivity {

    Context mContext;
    TextView txt_toolbar_title, txt_news_header, txt_local_news, txt_regional_news, txt_global_news;
    LinearLayout ll_news_back, ll_local_news, ll_regional_news, ll_global_news;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);


        mContext = this;
        setView();
        SetFonts();
        SetListner();
    }


    private void setView(){

        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);

        txt_news_header = findViewById(R.id.txt_news_header);

        txt_local_news = findViewById(R.id.txt_local_news);
        txt_regional_news = findViewById(R.id.txt_regional_news);
        txt_global_news = findViewById(R.id.txt_global_news);

        ll_news_back = findViewById(R.id.ll_news_back);
        ll_local_news = findViewById(R.id.ll_local_news);
        ll_regional_news = findViewById(R.id.ll_regional_news);
        ll_global_news = findViewById(R.id.ll_global_news);
    }

    private void SetFonts(){

        txt_toolbar_title.setTypeface(getMediumFonts());

        txt_news_header.setTypeface(getSemiBoldFonts());

        txt_local_news.setTypeface(getRegularFonts());
        txt_regional_news.setTypeface(getRegularFonts());
        txt_global_news.setTypeface(getRegularFonts());
    }

    private void SetListner(){

        ll_news_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        ll_local_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.kwapinspire.com.my/news/local/")));
            }
        });

        ll_regional_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.kwapinspire.com.my/news/regional/")));
            }
        });

        ll_global_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.kwapinspire.com.my/news/global/")));
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }


}
