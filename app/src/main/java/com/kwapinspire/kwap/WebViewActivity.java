package com.kwapinspire.kwap;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;

/**
 * Created by My 7 on 09-Jun-18.
 */

public class WebViewActivity extends BaseActivity
{
    WebViewActivity mContext;

    WebView webview_polls;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_polls);

        mContext = this;

        webview_polls = findViewById(R.id.webview_polls);

        webview_polls.getSettings().setJavaScriptEnabled(true); // enable javascript

        final Activity activity = this;

        webview_polls.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        webview_polls .loadUrl(BaseActivity.webViewLink);
    }

    @Override
    public void onBackPressed(){
        if(!webview_polls.canGoBack()){
            // If web view have back history, then go to the web view back history
            startActivity(new Intent(mContext,DashBoardActivity.class));
            finish();
        }else {
            // Ask the user to exit the app or stay in here
            startActivity(new Intent(mContext,DashBoardActivity.class));
            finish();
        }
    }
}
