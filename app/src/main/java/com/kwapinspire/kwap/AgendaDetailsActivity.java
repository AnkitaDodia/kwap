package com.kwapinspire.kwap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kwapinspire.kwap.adapters.AgendaSpeakerAdapter;
import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.model.AgendaDetails;
import com.kwapinspire.kwap.model.AgendaSpeakers;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 16-May-18.
 */

public class AgendaDetailsActivity extends BaseActivity
{
    AgendaDetailsActivity mContext;

    boolean isInternet;

    LinearLayout ll_agenda_back;

    ImageView img_agenda_details, img_agenda_speaker;

    TextView txt_toolbar_title, txt_agenda_title, txt_agenda_time, txt_agenda_date, txt_agenda_discription,
            txt_agenda_speaker_title, txt_agenda_speaker, txt_agenda_speaker_dis,text_divider;

    ProgressBar agenda_details_progress,agenda_progress;

    RecyclerView rv_agendaspeakers;

    ArrayList<AgendaSpeakers> AgendaSpeakersList = new ArrayList<>();

    WebView webview_agenda_speaker_dis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_detail);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListener();

        if (isInternet) {
            getAgendaDetailsRequest();
        } else {
            Toast.makeText(this, "Please connect to the internet to view agenda", Toast.LENGTH_SHORT).show();
        }

    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        ll_agenda_back = findViewById(R.id.ll_agenda_back);

        img_agenda_details = findViewById(R.id.img_agenda_details);
        img_agenda_speaker  = findViewById(R.id.img_agenda_speaker);

        txt_agenda_title = findViewById(R.id.txt_agenda_title);
        txt_agenda_time = findViewById(R.id.txt_agenda_time);
        txt_agenda_date = findViewById(R.id.txt_agenda_date);
        txt_agenda_discription = findViewById(R.id.txt_agenda_discription);
        txt_agenda_speaker = findViewById(R.id.txt_agenda_speaker);
        txt_agenda_speaker_dis  = findViewById(R.id.txt_agenda_speaker_dis);
        txt_agenda_speaker_title = findViewById(R.id.txt_agenda_speaker_title);
        text_divider = findViewById(R.id.text_divider);

        agenda_details_progress = findViewById(R.id.agenda_details_progress);
        agenda_progress = findViewById(R.id.agenda_progress);

//        webview_agenda_speaker_dis = findViewById(R.id.webview_agenda_speaker_dis);

        txt_toolbar_title.setTypeface(getMediumFonts());
        txt_agenda_title.setTypeface(getSemiBoldFonts());
        txt_agenda_time.setTypeface(getSemiBoldFonts());
        txt_agenda_date.setTypeface(getSemiBoldFonts());
        txt_agenda_speaker_title.setTypeface(getSemiBoldFonts());
        txt_agenda_discription.setTypeface(getLightFonts());

        rv_agendaspeakers = findViewById(R.id.rv_agendaspeakers);
    }

    private void setListener() {
        ll_agenda_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_agendaspeakers.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_agendaspeakers, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.SpeakerId = AgendaSpeakersList.get(position).getSpeakerId();

                Intent it = new Intent(mContext, SpeakersDetailsActivity.class);
                mContext.startActivity(it);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void getAgendaDetailsRequest() {
        try {
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getAgendaDetails(BaseActivity.AgendaId, new Callback<AgendaDetails>() {
                @Override
                public void success(AgendaDetails model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("AGENDA_DETAILS_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus() == 1) {

                                AgendaSpeakersList = model.getSpeakers();
                                setListData(model);
                            }
                            else
                            {
                                Toast.makeText(mContext,""+model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                    Toast.makeText(mContext,"Please connect to the internet to view agenda!!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListData(AgendaDetails model)
    {
        String ActDate = null;

        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(model.getDate());
            SimpleDateFormat newDate = new SimpleDateFormat("dd MMM yyyy");//set format of new date
            ActDate = newDate.format(date);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        String time = model.getFromTime()+" - "+model.getToTime();

        txt_agenda_title.setText(model.getTitle());
        txt_agenda_time.setText("Time: "+time);
        txt_agenda_date.setText("Date: "+ActDate);
        txt_agenda_discription.setText(model.getDescription());

        Log.e("DISCRIPTION",""+model.getDescription());

        Glide.with(mContext)
                .load(model.getImage())
                .error(R.drawable.ic_image_holder)
                .listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                agenda_progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                agenda_progress.setVisibility(View.GONE);
                return false;
            }
        }).into(img_agenda_details);

        if(AgendaSpeakersList.size() > 0)
        {
            rv_agendaspeakers.setVisibility(View.VISIBLE);
            text_divider.setVisibility(View.VISIBLE);
            txt_agenda_speaker_title.setVisibility(View.VISIBLE);

            AgendaSpeakerAdapter mAgendaSpeakerAdapter = new AgendaSpeakerAdapter(mContext, AgendaSpeakersList);
            rv_agendaspeakers.setLayoutManager(new LinearLayoutManager(mContext));
            rv_agendaspeakers.getLayoutManager().setMeasurementCacheEnabled(false);
            rv_agendaspeakers.setAdapter(mAgendaSpeakerAdapter);
        }
        else
        {
            rv_agendaspeakers.setVisibility(View.GONE);
            text_divider.setVisibility(View.GONE);
            txt_agenda_speaker_title.setVisibility(View.GONE);
        }
    }
}
