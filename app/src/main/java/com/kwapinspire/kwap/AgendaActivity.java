package com.kwapinspire.kwap;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kwapinspire.kwap.common.BaseActivity;
import com.kwapinspire.kwap.fragments.Day1AgendaFragment;
import com.kwapinspire.kwap.fragments.Day2AgendaFragment;
import com.kwapinspire.kwap.model.Agendas;
import com.kwapinspire.kwap.restinterface.RestInterface;
import com.kwapinspire.kwap.utility.InternetStatus;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AgendaActivity extends BaseActivity {

    AgendaActivity mContext;

    private TabLayout tabLayout;

    private ViewPager viewPager;

    LinearLayout ll_parent_agenda, ll_agenda_back;
    String msg;

    TextView txt_toolbar_title;
    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListners();

        if (isInternet) {
            getAgendaRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        ll_parent_agenda = (LinearLayout)findViewById(R.id.ll_parent_agenda);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        ll_agenda_back  = (LinearLayout)findViewById(R.id.ll_agenda_back);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0cb4cf"));
        tabLayout.setTabTextColors(Color.parseColor("#434343"), Color.parseColor("#434343"));

        txt_toolbar_title.setTypeface(getMediumFonts());
    }

    private void setListners(){

        ll_agenda_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager)
    {
        String agendaTitle1 =  mContext.getString(R.string.agenda_tab_title1);
        String agendaTitle2 =  mContext.getString(R.string.agenda_tab_title2);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Day1AgendaFragment(),agendaTitle1);
        adapter.addFragment(new Day2AgendaFragment(), agendaTitle2);
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getAgendaRequest()
    {
        try {
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getAgenda(new Callback<Agendas>() {
                @Override
                public void success(Agendas model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("AGENDA_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                BaseActivity.mDay1List = model.getData().getDay1();
                                BaseActivity.mDay2List = model.getData().getDay2();
                            }
                            else
                            {
                                msg = model.getMessage();
                            }

                            setupViewPager(viewPager);

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
        super.onBackPressed();
    }
}
